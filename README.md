# xevent

Generic Event Handler for small devices

## Introduction

`xevent` is a leightweight automation frame work implemented in C++. It consists of a generic part that organizes
the communication via events and a domain specific part that does the actual work.

## Description

Many technical tasks require an asynchronous handling of events. These events origin from timers, interrupts, 
or external input.  They initiate a work flow. Some calls within this work-flow might be also synchronous, 
such as fetching a measurement value.

`xevent` is a light weight automation framework implemented in C++. Task specific code is simply added by 
additional shared libraries. A xevent process is started for each shared library as defined in the configuration 
file.

The  configuration  file  defines  the  following items:

- The workflow with the participating object classes
- `xevent` processes with the corresponding shared libraries
- The distribution of the object classes to the shared libraries / event handler processes
- Timers and interrupts

The workflow has to be built from standard event types: `GET,SET CREATE,DELETE,TRIGGER,RESPONSE`. The
`RESPONSE` event is used for the responses of synchronous calls. Participating object classes have to implement 
the events that are used with this class.

The generic part provides the following object classes:

- `Data Provider` is used for data calibration. Measurement values can be mapped with a calibration curve.
- `Logger` writes data to a log file.
- `SystemCall` performs calls to the operating system.
- Classes to perform fuzzy logic.

An application has to create a logic tree, that is built by fuzzy operations (e.g.`NOT,AND,OR`) and
comparator leaves. Fuzzy logic allows to implement expressions from natural speech like ”in the night”: 
This may be defined by:

- ”completely in the night”: 11 pm to 5 am, resulting in a 100% value
- ”not in the night”: 9 am to 5 pm, resultingin a 0% value
- The time in between results in percentages by linear interpolation.

Typical use of this software are all kinds of steering tasks like operating cooling and heating, where it is 
already in use, or steering tasks in a production environment.

## Compilation

`xevent` is shipped with `cmake` support. The easyest way to compile this software is the use of 
[`mbuild`](http://wp.wagnertech.de/?page_id=370). `mbuild` provides support for cross compiling.

## Installation

Precompiled packages (`deb` format) are available [here](https://hilfe.wagnertech.de/index.php/installation-von-wagnertech-software/).

