/*
 * DataProvider.h
 *
 * Handled object for calibrating measurement data.
 * Implemented events: see below
 *
 */

#ifndef DATAPROVIDER_H_
#define DATAPROVIDER_H_

#include "EDCommon.h"
#include "HandledObject.h"

// a user object has to be derived from HandledObject
class DataProvider: public HandledObject {
public:
	// Method used by HandlerFactory.cpp
	static DataProvider& getInstance(const EDCommon&, int id);

	// Constructor used by getInstance()
	DataProvider(int id, int delay, const std::string& fileName);

	/*
		Implementation of GET event.
		Expected IN data: integer value of uncalibrated measurement
		If inside the delay time:
			OUT data: float value of calibrated measurement, if inside the delay time
			Return value: 0
		If outside the delay time:
			Return value: 1
	*/
	virtual int get(Event&, Event&);
	/*
		Implementation of RESPONSE event.
		Expected IN data: integer value of uncalibrated measurement
		OUT data: float value of calibrated measurement, if inside the delay time
		Return value: 0
	*/
	virtual int response(Event&, Event&);

private:
	static vector<DataProvider> instances;

	vector<pair<int,float> > calib_data;
	int id;
	float last_val;
	time_t last_update;
	int max_delay;
};

#endif /* DATAPROVIDER_H_ */
