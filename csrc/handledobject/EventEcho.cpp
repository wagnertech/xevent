/*
 * EventEcho.cpp
 *
 *  Created on: 27.09.2016
 *      Author: gnublin
 */

#include "EventEcho.h"

#include "EventException.h"

#include <sstream>

EventEcho::EventEcho() {
}

EventEcho::~EventEcho() {
}
/*
 * EventEcho: Returns the integer given as data back as return value
 * The integer can be given as:
 * - integer type
 * - String in the Format: "Result:<value>"
 */
int EventEcho::trigger(Event& e, Event&) {

	int rc;
	if (e.getDataLength() > (int)sizeof(int)) {
		// seems to be a string
		stringstream strstr;
		string data(&e.getData());
		strstr << data.substr(7);
		strstr >> rc;
	}
	else if (e.getDataLength() == (int)sizeof(int)) {
		// int parameter
		int* p = (int*) &e.getData();
		rc = *p;
	}
	else throw EventException("invalid data length", e);
	return rc;
}

EventEcho EventEcho::theInstance;
