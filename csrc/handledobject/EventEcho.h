/*
 * EventEcho.h
 *
 * This class returns on a trigger event the number given
 * in the data. The data is expected
 * - as String: "String: <number>
 *  or
 * - as Integer
 */

#ifndef EVENTECHO_H_
#define EVENTECHO_H_

#include <HandledObject.h>

class EventEcho: public HandledObject {
public:
	EventEcho();
	virtual ~EventEcho();

	static EventEcho theInstance;

	virtual int trigger(Event&, Event&);
};

#endif /* EVENTECHO_H_ */
