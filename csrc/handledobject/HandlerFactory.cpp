/*
 * HandlerFactory.cpp
 *
 *  Created on: 29.01.2014
 *      Author: gnublin
 */

#include "HandlerFactory.h"

#include "DataProvider.h"
#include "EventEcho.h"
#include "EventException.h"
#include "Logger.h"
#include "SystemCall.h"
#include "Trigger.h"

#include <stdexcept>

static int LOGGER_ID = -1;
static int ECHO_ID = -1;
static int DATA_PROVIDER_ID = -1;
static int SYSTEM_CALL_ID = -1;
static int TRIGGER_ID = -1;

HandledObject& HandlerFactory::getInstance(int i, int j) {
	if (i == LOGGER_ID) ;
	else if (i == ECHO_ID) return EventEcho::theInstance;
	else if (i == DATA_PROVIDER_ID) return DataProvider::getInstance(*edCommon, j);
	else if (i == SYSTEM_CALL_ID) return SystemCall::getInstance(*edCommon, j);
	else if (i == TRIGGER_ID) return Trigger::getInstance(*edCommon, j);
	else {
		if (i < (int)this->getClassList().size()) {
			std::string msg = string("unknown class: ")+this->getClassList().at(i);
			throw EventException(msg.c_str());
		}
		else throw EventException("invalid class id: ", i);
	}
	return *Logger::getInstance();
}

void HandlerFactory::setClassIds(){
	const t_class_list& classList = this->getClassList();
	for (size_t i=0; i<classList.size(); i++) {
		if (classList[i] == "Logger") LOGGER_ID = i;
		else if (classList[i] == "EventEcho") ECHO_ID = i;
		else if (classList[i] == "DataProvider") DATA_PROVIDER_ID = i;
		else if (classList[i] == "SystemCall") SYSTEM_CALL_ID = i;
		else if (classList[i] == "Trigger") TRIGGER_ID = i;
	}
}
