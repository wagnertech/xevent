/*
 * Logger.cpp
 *
 *  Created on: 03.04.2014
 *      Author: gnublin
 */

#include <string.h> // memcpy

#include "Logger.h"

Logger* Logger::theInstance = 0;

Logger::Logger() {
	this->ofile.open("/tmp/objecthandler.log",std::ios_base::out);
	this->ofile << "Programm start" << std::endl;
}

int Logger::trigger(Event& event, Event&){
	this->ofile << "Event: " << event << std::endl;
	return 0;
}

Logger* Logger::getInstance(){
	if(!Logger::theInstance) {
		Logger::theInstance = new Logger();
	}

	return Logger::theInstance;
}
