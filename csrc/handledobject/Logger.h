/*
 * Logger.h
 *
 * Handled object for writing into a log file /tmp/objecthandler.log
 * Logger is implemented as singleton.
 *
 */

#ifndef LOGGER_H_
#define LOGGER_H_

#include <iostream>
#include <fstream>

#include <HandledObject.h>

class Logger: public HandledObject {
public:
	// Method used by HandlerFactory
	static Logger* getInstance();

	/*
	 * trigger writes data into the log file
	 * Expected data: String data
	 * Returns: 0
	 */
	virtual int trigger(Event&, Event&);

private:
	Logger();
	static Logger* theInstance;
	std::ofstream ofile;
};

#endif /* LOGGER_H_ */
