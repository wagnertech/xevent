/*
 * SystemCall.cpp
 *
 *  Created on: 23.12.2017
 *      Author: gnublin
 */

#include "ConfigFile.h"
#include "EventException.h"
#include "SystemCall.h"

#include <cstring>
#include <cstdlib>
#include <sstream>
#include <vector>

using namespace std;

SystemCall SystemCall::theInstance;
vector<SystemCall> SystemCall::instances;

int SystemCall::trigger(Event& e, Event&) {
	stringstream ss;
	if (this->base_command.size() > 0) ss << this->base_command << " ";
	if (e.getDataType() == 's') {
		char* cmd = &e.getData();
		ss << cmd;
	}
	else if (e.getDataType() == 'i') {
		int data;
		memcpy(&data, &e.getData(), e.getDataLength());
		ss << data;
	}
	else throw EventException("Data type not implemented: ", e);
	return system(ss.str().c_str());
}

SystemCall& SystemCall::getInstance(const EDCommon& edCommon, int id) {
	if (id == 0) return theInstance;
	if (SystemCall::instances.size() == 0) {
		// load instances

		// get config file name
		string fileName = edCommon.getConfigFileName();

		// get data provider content
		ConfigFile cf(fileName);
		vector<string> lines = cf.requireSectionData("SystemCall");
		for (auto& line : lines) {
			SystemCall::instances.push_back(SystemCall(line));
		}
	}
	return SystemCall::instances.at(id-1);


}
