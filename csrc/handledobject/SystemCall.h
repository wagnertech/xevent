/*
 * SystemCall.h
 *
 * On trigger event the given string data is executes as system call.
 *
 */

#ifndef SYSTEMCALL_H_
#define SYSTEMCALL_H_

#include <HandledObject.h>

#include "EDCommon.h"

class SystemCall: public HandledObject {

private:
	std::string base_command;

	SystemCall(std::string base_command = "")
	:base_command(base_command) {}

public:
	// static instance instantiated at system start
	static SystemCall theInstance;

	/*
	 * Fabric method.
	 * Logic on instance id:
	 *   0: "theInstance" is returned
	 *      the given string is executed
	 *  >0: instance is created. Base command
	 *      string is taken from configuration
	 *      given data is appended to the command string
	 * [SystemCall]
	 * base command1
	 * base command2
	 */
	static SystemCall& getInstance(const EDCommon&, int id);

	/*
	 * trigger performs a system call.
	 * Expected data: String with the command line (for instance 0)
	 *                Any data appended to base command
	 * Returns: The return value of the system call
	 */
	virtual int trigger(Event&, Event&);

private:
	static std::vector<SystemCall> instances;
};

#endif /* SYSTEMCALL_H_ */
