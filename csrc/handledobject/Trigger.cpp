/*
 * Trigger.cpp
 *
 *  Created on: 03.08.2021
 *      Author: debian10
 */

#include "Trigger.h"

#include "ConfigFile.h"
#include "EventException.h"
#include "util.h"

#include <string.h>

using namespace std;

vector<Trigger> Trigger::instances;

int Trigger::set(Event& in) {
	if (in.getDataType() != 'f') throw EventException("Trigger: Type f expected.", in);
	float curr_val;
	memcpy(&curr_val, &in.getData(), sizeof(float));
	if (!this->status && curr_val > this->upper) {
		this->status = true;
		return 1;
	}
	else if (this->status && curr_val < this->lower) {
		this->status = false;
		return -1;
	}
	return 0;
}

Trigger& Trigger::getInstance(const EDCommon& edCommon, int id) {
	if (Trigger::instances.size() == 0) {
		// load instances

		// get config file name
		string fileName = edCommon.getConfigFileName();

		// get data provider content
		ConfigFile cf(fileName);
		vector<string> lines = cf.requireSectionData("Trigger");
		for (size_t i=0; i<lines.size(); i++) {
			float l, u;
			util::split(lines[i], ';', l, u);
			Trigger::instances.push_back(Trigger(l, u));
		}
	}
	return Trigger::instances.at(id-1);

}
