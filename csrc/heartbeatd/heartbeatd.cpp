//============================================================================
// Name        : heartbeatd.cpp
// Author      : WTG
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <string>
#include <iostream>
#include <stdexcept>
#include <stdlib.h> // for atoi
#include <unistd.h> // usleep
//#include <thread>         // std::this_thread::sleep_for
//#include <chrono>         // std::chrono::seconds

#include "DistributionRecord.h"
#include "EDCommon.h"
#include "Event.h"
#include "HandlerRecord.h"

using namespace std;

int main(int argc, char *argv[]) {

	// check parameters
	const char* usage = "heatbeatd <ms-time|0> [config file] [queue name]";
	if(argc == 1) {
		std::cerr << "parameter missing. usage: " << usage << std::endl;
		return 1;
	}

	int time = atoi(argv[1]);

	string configFileName;
	if(argc > 2) {
		configFileName = argv[2];
	}

	// define queue name
	string queueName;
	if(argc == 4) {
		queueName = argv[3];
	}

	try {

		// get helper class
		EDCommon edCommon(configFileName, queueName);

		// get class id of TimerMgr
		int timerMgrId = edCommon.requireClassId("TimerMgr");

		// get incarnation of timerhandler
		DistributionRecord& dr = edCommon.getDistributionRecord();
		int thInc = dr.getTgtHandler(timerMgrId, 1);

		// open queue
		edCommon.openQueue();

		// define message
		t_message qbuf;
		qbuf.tgtIncarnation = thInc;
		qbuf.sndIncarnation = -1;
		qbuf.event.evt_id = Event::EVT_TRIGGER;
		qbuf.event.length = 0;
		qbuf.event.snd_class = 0;
		qbuf.event.snd_inst = 0;
		qbuf.event.synchon = false;
		qbuf.event.tgt_class = timerMgrId;
		qbuf.event.tgt_inst = 1;
		qbuf.event.thread = 0;

		for (;;) {

			if (time == 0) {
				string dummy;
				cout << "Press <Enter> to trigger next event";
				getline(cin,dummy);
			}
			usleep(time*1000);
			//std::this_thread::sleep_for (std::chrono::milliseconds(time));
			qbuf.event.thread++;
			edCommon.sendEvent(qbuf);
		}

	}
	catch(runtime_error& e){
		cerr << "Exception: " << e.what();
	}
	catch(...){
		std::cerr << "anything failed." << std::endl;
	}
	return 0;
}
