/*
 * Board.cpp
 *
 *  Created on: 26.11.2014
 *      Author: c__11kurs
 */

#include "Board.h"
#include "EventException.h"

#include <string>
#include <stdexcept>

using namespace std;

Board& Board::getInstance() {
	return theBoard;
}

Gpio& Board::getGpio(int i) {
	this->gpio.setId(i);
	return this->gpio;
}

Led& Board::getLed(int i) {
	if (i == 1) return this->led1;
	throw EventException("unknown LED.", i);
}

Relais& Board::getRelais(int i) {
	switch (i) {
	case 1: return this->relais1;
	case 2: return this->relais2;
	case 3: return this->relais3;
	case 4: return this->relais4;
	default: throw EventException("unknown Relais.", i);
	}
}

Sarah& Board::getSarah(const EDCommon& edc, int i) {

	// singleton
	if (!this->sarah) {
		sarah = new Sarah(edc);
	}
	this->sarah->setId(i);
	return *this->sarah;
}



Board Board::theBoard;
