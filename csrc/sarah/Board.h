/*
 * Board.h
 *
 *  Created on: 26.11.2014
 *      Author: c__11kurs
 */

#ifndef GNUBLIN_BOARD_H_
#define GNUBLIN_BOARD_H_

#include "Gpio.h"
#include "Led.h"
#include "Relais.h"
#include "Sarah.h"

class Board {
public:
	static Board& getInstance();

	Gpio& getGpio(int i);
	Led& getLed(int i);
	Relais& getRelais(int i);
	Sarah& getSarah(const EDCommon& edc, int i);

private:
	Board():
#ifdef _ARMEL
	sarah(NULL) {}
#else
	sarah(nullptr) {}
#endif

	static Board theBoard;

	Led led1;

	Relais relais1;
	Relais relais2;
	Relais relais3;
	Relais relais4;

	Sarah* sarah;

	Gpio gpio;
};

#endif /* GNUBLIN_BOARD_H_ */
