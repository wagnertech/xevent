/*
 * Gpio.cpp
 *
 *  Created on: 01.02.2016
 *      Author: gnublin
 */

#include "EventException.h"
#include "Gpio.h"

#ifdef _ARMEL
	#include "gnublin.h"
#else
	#include <iostream>
#endif

Gpio::Gpio() : id(0){
}

Gpio::~Gpio() {
}

void Gpio::setId(int id) {
	this->id = id;
}

int Gpio::get(Event& evt, Event& out){

	// initializing result
	this->value = 1;
#ifdef _ARMEL
	gnublin_gpio gpio;
	gpio.pinMode(this->id,INPUT);
	value = gpio.digitalRead(this->id);
#else
	std::cout << "returning hardcoded 1" << std::endl;
#endif
	if (evt.getDataLength()>0) {
		char* data = &(evt.getData());
		if (data[0] == 's') {
			// string format requested
			const char* outData = "s1";
			if (this->value != 1) outData = "s0";
			out.pushData(outData, 2, 's');
			return 0;
		}
	}
	// binary mode
	out.pushData((char*)this, sizeof(this), 'o');
	return 0;
}

int Gpio::set(Event& evt) {

	// evaluation set-string
	if (evt.getDataLength() != 2) throw EventException("invalid data length", evt);
	char* data = &(evt.getData());
	if (data[0] != 's') throw EventException("invalid data format", evt);
	int result = -1;
	switch (data[1]) {
		case '1': this->value = 1;
			result = 1;
		break;
		case '0': this->value = 0;
			result = 0;
		break;
		default: throw EventException("invalid data value", evt);
	}
	#ifdef _ARMEL
		gnublin_gpio gpio;
		gpio.pinMode(this->id,OUTPUT);
		gpio.digitalWrite(this->id,result);
	#else
		if (result == 1) std::cout << "GPIO switched on." << std::endl;
		else std::cout << "GPIO switched off." << std::endl;
	#endif

	return result;
}
