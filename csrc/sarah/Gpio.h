/*
 * Gpio.h
 *
 *  Created on: 01.02.2016
 *      Author: gnublin
 */

#ifndef GPIO_H_
#define GPIO_H_

#include "HandledObject.h"
#include "Event.h"

class Gpio : public HandledObject {
public:
	Gpio();
	virtual ~Gpio();

	void setId(int);

	virtual int get(Event&, Event&);
	virtual int set(Event&);

private:
	int id;
	int value; // for intermediate storage
};

#endif /* GPIO_H_ */
