/*
 * HandlerFactory.cpp
 *
 *  Created on: 29.01.2014
 *      Author: gnublin
 */

#include "Aussentemperatur.h"
#include "Board.h"
#include "EventException.h"
#include "HandlerFactory.h"
#include "Heizung.h"
#include "HeizungRegelwerk.h"

#include <stdexcept>
#include <string>

static int LED_ID = -1;
static int RELAIS_ID = -1;
static int SARAH_ID = -1;
static int BOARD_ID = -1;
static int HEIZUNG_ID = -1;
//static int VORLAUF_ID = 0;
static int MANAGER_ID = -1;
static int GPIO_ID = -1;
static int HR_ID = -1;
static int AT_ID = -1;

HandledObject& HandlerFactory::getInstance(int classs, int inst) {
	if (classs == HEIZUNG_ID) return Heizung::getInstance();
	else if (classs == GPIO_ID) return Board::getInstance().getGpio(inst);
	else if (classs == HR_ID) return HeizungRegelwerk::getInstance(*this->edCommon);
	else if (classs == LED_ID) return Board::getInstance().getLed(inst);
	else if (classs == RELAIS_ID) return Board::getInstance().getRelais(inst);
	else if (classs == SARAH_ID) return Board::getInstance().getSarah(*this->edCommon, inst);
	else if (classs == AT_ID) return Aussentemperatur::getInstance();
	else {
		const t_class_list classList = this->edCommon->getClassList();
		if (classs < (int)classList.size()) {
			std::string msg = string("unknown class: ")+classList.at(classs);
			throw EventException(msg.c_str());
		}
		else throw EventException("invalid class id: ", classs);
	}
	return Board::getInstance().getLed(inst);
}

void HandlerFactory::setClassIds(){
	const t_class_list& classList = this->getClassList();
	for (int i=0; i<(int)classList.size(); i++) {
		if (classList[i] == "Led") LED_ID = i;
		else if (classList[i] == "Relais") RELAIS_ID = i;
		else if (classList[i] == "Sarah") SARAH_ID = i;
		else if (classList[i] == "Board") BOARD_ID = i;
		else if (classList[i] == "Heizung") HEIZUNG_ID = i;
//		else if (classList[i] == "Vorlauf") VORLAUF_ID = i;
		else if (classList[i] == "Manager") MANAGER_ID = i;
		else if (classList[i] == "Gpio") GPIO_ID = i;
		else if (classList[i] == "HeizungRegelwerk") HR_ID = i;
		else if (classList[i] == "Aussentemperatur") AT_ID = i;
	}
}
