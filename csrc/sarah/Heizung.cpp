/*
 * Heizung.cpp
 *
 *  Created on: 04.11.2015
 *      Author: gnublin
 */

#include "EventException.h"
#include "Heizung.h"

#include <string.h>

Heizung Heizung::theInstance;

// Konstanten für die internen Daten
const char* Heizung::VLT = "VLT"; // vorlaufTemperatur
const int Heizung::VLT_REQ = 1;
//const char* Heizung::AUT = "AUT"; // aussenTemperatur
//const char* Heizung::HGR = "Heizgrad";
//const int Heizung::HGR_NR = 1;
const char* Heizung::HZT = "Heizzeit";
const int Heizung::HZT_REQ = 11;
const int Heizung::HZT_RSP = 12;

Heizung& Heizung::getInstance() {
	return theInstance;
}

int Heizung::get(Event& evt, Event& out) {
	if (strncmp(&(evt.getData()), Heizung::HZT, evt.getDataLength()) == 0) {
		time_t now = time(0);
		if (now-heizzeit_stmp > maxDelay) return Heizung::HZT_REQ;
		out.pushData((char*)&this->heizzeit, sizeof(int), 'i');
		return 0;
	}
	else if (strncmp(&(evt.getData()), Heizung::VLT, evt.getDataLength()) == 0) {
		time_t now = time(0);
		if (now-heizzeit_stmp > maxDelay) return Heizung::VLT_REQ;
		out.pushData((char*)&this->vlt, sizeof(float), 'f');
		return 0;
	}
	else throw EventException("unkown GET data in Heizung::get");
	return 0;
}

int Heizung::trigger(Event& evt, Event&) {
/*
	if (strncmp(&(evt.getData()), Heizung::HGR, evt.getDataLength()) == 0) {
		outLength = 0;
		this->eventMap[evt.getThread()] = Heizung::HGR_NR;
		return Heizung::HGR_NR;
	}
	else
	*/
	throw runtime_error("Heizung::trigger not implemented");
	return 0;
}

int Heizung::set(Event& evt) {
/*
	// evaluation set-string
	if (evt.getDataLength() != 2) throw EventException("invalid data length", evt);
	char* data = &(evt.getData());
	if (data[0] != 'a') throw EventException("invalid data format", evt);
	int result = -1;
	switch (data[1]) {
		case 't': this->aktiv = true;
			result = 1;
		break;
		case 'f': this->aktiv = false;
			result = 0;
		break;
		default: throw EventException("invalid data value", evt);
	}
*/
	throw runtime_error("Heizung::set not implemented");
	return 0;
}

int Heizung::response(Event& evt, Event& out) {
	// save data
	memcpy(&this->vlt, &evt.getData(), sizeof(float));
	this->vlt_stmp = time(0);
	out.pushData((char*)&this->vlt, sizeof(float), 'f');
	return 0;
}

