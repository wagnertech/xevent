/*
 * Heizung.h
 *
 *  Created on: 04.11.2015
 *      Author: gnublin
 */

#ifndef HEIZUNG_H_
#define HEIZUNG_H_

#include "HandledObject.h"
#include "FuzzyNumber.h"

#include <map>

class Heizung : public HandledObject {
public:
	static Heizung& getInstance();

	virtual int get(Event&, Event&);
	virtual int trigger(Event&, Event&);
	virtual int set(Event&);
	virtual int response(Event&, Event&);

	// Konstanten für die internen Daten
	static const char* HZT;
	static const int HZT_REQ;
	static const int HZT_RSP;
	static const char* VLT;
	static const int VLT_REQ;

private:
	static Heizung theInstance;
	float vlt;
	int heizzeit; // min Brennerzeit pro 15 min
	time_t heizzeit_stmp;
	int maxDelay;
	time_t vlt_stmp;
	std::map<int,Event> eventMap;
	Heizung()
		: heizzeit(0),
		  heizzeit_stmp(0),
		  maxDelay(3600),
		  vlt_stmp(0) {}
};

#endif /* HEIZUNG_H_ */
