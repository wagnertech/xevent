/*
 * HeizungRegelwerk.cpp
 *
 *  Created on: 06.09.2016
 *      Author: gnublin
 */

#include "HeizungRegelwerk.h"

#include <ValueLeaf.h>
#include "FuzzyNode.h"
#include "TimeLeaf.h"

using namespace xfuzz;

HeizungRegelwerk* HeizungRegelwerk::theInstance;

HeizungRegelwerk::HeizungRegelwerk(const EDCommon& edcommon) {
	this->init(edcommon, "HeizungRegelwerk");
	// Nacht && sehr kalt
	/* TODO
	ActiveLeaf* sehrKalt = new ActiveLeaf(1, -5., -12., 300);
	TimeLeaf* nacht = new TimeLeaf(22., 30.);
	FuzzyNode* nachtheiz = new FuzzyNode(AND, sehrKalt, nacht);

	// Tag && kalt
#ifdef _ARMEL
	FuzzyNode* tag = new FuzzyNode(NOT, nacht, NULL);
#else
	FuzzyNode* tag = new FuzzyNode(NOT, nacht, nullptr);
#endif
	ActiveLeaf* kalt = new ActiveLeaf(1, 10., 0., 300);
	FuzzyNode* tagheiz = new FuzzyNode(AND, kalt, tag);

	// Morgen && kühl
	ActiveLeaf* kuhl = new ActiveLeaf(1, 15., 10., 300);
	TimeLeaf* morgen = new TimeLeaf(6., 8.);
	FuzzyNode* morgenheiz = new FuzzyNode(AND, kuhl, morgen);

	// gesamt = nachtheiz || tagheiz || morgenheiz
	FuzzyNode* or1 = new FuzzyNode(OR, nachtheiz, tagheiz);
	FuzzyNode* or2 = new FuzzyNode(OR, or1, morgenheiz);
	this->baseNode = or2;
	*/
}

HeizungRegelwerk& HeizungRegelwerk::getInstance(const EDCommon& edcommon) {
	if (!HeizungRegelwerk::theInstance) HeizungRegelwerk::theInstance = new HeizungRegelwerk(edcommon);
	return *HeizungRegelwerk::theInstance;
}
