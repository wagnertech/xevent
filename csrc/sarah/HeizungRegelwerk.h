/*
 * HeizungRegelwerk.h
 *
 *  Created on: 06.09.2016
 *      Author: gnublin
 */

#ifndef HEIZUNGREGELWERK_H_
#define HEIZUNGREGELWERK_H_

#include "RuleSystem.h"

class HeizungRegelwerk : public xfuzz::RuleSystem{
public:
	HeizungRegelwerk(const EDCommon&);

	static HeizungRegelwerk& getInstance(const EDCommon&);

private:
	static HeizungRegelwerk* theInstance;
};

#endif /* HEIZUNGREGELWERK_H_ */
