/*
 * Led.cpp
 *
 *  Created on: 26.11.2014
 *      Author: c__11kurs
 */

#include "EventException.h"
#include "Led.h"

#ifdef _ARMEL
	#include "gnublin.h"
#else
	#include <iostream>
#endif

Led::Led(bool status): status(status) {}

int Led::get(Event& evt, Event& out){
	if (evt.getDataLength()>0) {
		char* data = &(evt.getData());
		if (data[0] == 's') {
			// string format requested
			const char* outData = "sf";
			if (this->status) outData = "st";
			out.pushData(outData, 2, 's');
			return 0;
		}
	}
	// binary mode
	out.pushData((char*)this, sizeof(*this), 'o');
	return 0;
}
int Led::set(Event& evt) {

	// evaluation set-string
	if (evt.getDataLength() != 2) throw EventException("invalid data length", evt);
	char* data = &(evt.getData());
	if (data[0] != 's') throw EventException("invalid data format", evt);
	int result = -1;
	switch (data[1]) {
		case 't': this->status = true;
			result = 1;
		break;
		case 'f': this->status = false;
			result = 0;
		break;
		default: throw EventException("invalid data value", evt);
	}
	#ifdef _ARMEL
		gnublin_gpio gpio;
		gpio.pinMode(3,OUTPUT);
		gpio.digitalWrite(3,result);
	#else
		if (result == 1) std::cout << "LED switched on." << std::endl;
		else std::cout << "LED switched off." << std::endl;
	#endif

	return result;
}


