/*
 * Led.h
 *
 *  Created on: 26.11.2014
 *      Author: c__11kurs
 */

#ifndef GNUBLIN_LED_H_
#define GNUBLIN_LED_H_

#include "HandledObject.h"
#include "Event.h"

class Led : public HandledObject{
public:
	Led(bool);
	Led() : status(false){}
	virtual int get(Event&, Event&);
	virtual int set(Event&);
/*
	union LedData {
		char data[300];
		bool status;
	} LedData;
*/
private:
	bool status;
};

#endif /* GNUBLIN_LED_H_ */
