/*
 * Relais.cpp
 *
 *  Created on: 04.11.2015
 *      Author: gnublin
 */

#include "EventException.h"
#include "Relais.h"

#ifdef _ARMEL
	#include "util.h"
	#include <cstdlib>
#else
	#include <iostream>
#endif


int Relais::set(Event& evt) {

	// evaluation set-string
	if (evt.getDataLength() != 2) throw EventException("invalid data length", evt);
	char* data = &(evt.getData());
	if (data[0] != 's') throw EventException("invalid data format", evt);
	int result = -1;
	switch (data[1]) {
		case 't': this->status = true;
			result = 1;
		break;
		case 'f': this->status = false;
			result = 0;
		break;
		default: throw EventException("invalid data value", evt);
	}
	int instance = evt.getTgtInst();
	if (instance < 1 || instance > 4) throw EventException("invalid target instance", evt);
	#ifdef _ARMEL
		//gnublin_module_relay relais(0x20);
		//relais.switchPin(3,result);
		string cmd = "gnublin-relay -p " + util::to_string(instance) + " -o "+util::to_string(result);
		system(cmd.c_str());
	#else
		if (result == 1) std::cout << "Relais switched on: " << instance << std::endl;
		else std::cout << "Relais switched off: " << instance << std::endl;
	#endif

	return result;
}
