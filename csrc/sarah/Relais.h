/*
 * Relais.h
 *
 *  Created on: 04.11.2015
 *      Author: gnublin
 */

#ifndef RELAIS_H_
#define RELAIS_H_

#include <HandledObject.h>

class Relais: public HandledObject {
public:
	Relais():status(false){}
	virtual int set(Event&);

private:
	bool status;
};

#endif /* RELAIS_H_ */
