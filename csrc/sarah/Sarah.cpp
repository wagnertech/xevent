/*
 * program to handle ZMD usb starterkit SARah; based on libusb example dpfp.c.
 */

#include "Sarah.h"

#include "ConfigFile.h"

#include <stdexcept>
#include <string>
#include <vector>

Sarah::Sarah(const EDCommon& edc)
: lastUpdate(0)
, testmode (false)
, maxTime (10)
{
	// get config file name
	string fileName = edc.getConfigFileName();

	// get confiruration
	ConfigFile cf(fileName);
	vector<string> lines = cf.requireSectionData("Sarah");
#ifdef _ARMEL
	for (vector<string>::iterator i=lines.begin(); i!=lines.end(); i++){
		if (*i == "Test") this->testmode = true;
	}
#else
	for (auto line : lines) {
		if (line == "Test") this->testmode = true;
	}
#endif
	if (!testmode) this->init();
}

int Sarah::get(Event& evt, Event& out) {
	if (time(0) - this->lastUpdate > this->maxTime) this->measure();
	out.pushData((char*)&(this->lastResult[this->id]), sizeof(unsigned int), 'i');
	return 0;
}

void Sarah::setId(int id) {
	this->id = id;
}

void Sarah::init(){
	struct usb_device * dev = NULL;
	int 		r = 0;

	if (this->testmode) return;

	usb_init();

//	usb_set_debug(3);

	if ((dev = find_device_with_vid_pid(0x1336, 0x2001)) == NULL)
		throw runtime_error("device not found");

	if ((mDeviceHandle = usb_open(dev)) == NULL)
		throw runtime_error("no usb_open");

	if ((r = usb_claim_interface(mDeviceHandle, 0)) < 0)
		throw runtime_error("usb_claim_interface error");
}

void Sarah::measure(){
	int ch_no;
	uint8_t	lm_rqu[13]={0x81,0x00,0x08,0x00,0x8f,0xcf,0x9f,0xdf,0xaf,0xef,0xbf,0xff,0x0A};
	char	lm_rsp[0x40];
	int 	r;
	unsigned int i;
//	int pdcm = 3;
//	int sgl = 1; // single ended
//	int uni = 1; // unipolar
//	int ch_cmd = pdcm + 4*sgl + 8*uni;

	if(this->testmode) {
		for (ch_no=0; ch_no<8; ch_no++)	this->lastResult[ch_no] = ch_no*100;
		//for (ch_no=0; ch_no<8; ch_no++)	this->lastResult[ch_no] = lm_rsp[4 + 4*ch_no + 1] * 256 + lm_rsp[4 + 4*ch_no];
		return;
	}

//	for (ch_no=0; ch_no<8; ch_no++){
//		lm_rqu[ch_no + 4] -= (0xf - ch_cmd);
//	}

	const char* input = (const char*)lm_rqu;

	for (i=0; i<2; i++){
		r = usb_bulk_write(
			mDeviceHandle,
			1, //EP
			input,
			sizeof(lm_rqu),
			100 ); //timeout
		if (r < 0) throw runtime_error("lm request bulk error");

		// wait 200 ms (minimum 100 ms)
		usleep(200000);

		// read interface
		r = usb_bulk_read(
			mDeviceHandle,
			2, //EP
			lm_rsp,
			sizeof(lm_rsp),
			100); //timeout
		if (r < 0) throw runtime_error("lm response bulk error");
	}

	if (r < 36) throw runtime_error("lm response incomplete");

	for (ch_no=0; ch_no<8; ch_no++)	this->lastResult[ch_no] = lm_rsp[4 + 4*ch_no + 1] * 256 + lm_rsp[4 + 4*ch_no];

}
struct usb_device * Sarah::find_device_with_vid_pid(
	uint16_t vendor_id,
	uint16_t product_id)
{
	struct usb_bus    *bus;
	struct usb_device *dev;
	int r;

	r = usb_find_busses();
	if (r < 0) throw runtime_error("usb_find_busses error");

	r = usb_find_devices();
	if (r < 0) throw runtime_error("usb_find_devices error error");

	bus = usb_get_busses();

	while (bus != NULL) {
		dev = bus->devices;
		while (dev != NULL) {
			if (dev->descriptor.idVendor == vendor_id &&
				dev->descriptor.idProduct == product_id)
				return dev;
			dev = dev->next;
		}
		bus = bus->next;
	}

	return NULL;
}
/*

static int print_lm_string_bulk(
	usb_dev_handle *devh,
	int ch_no,
	int ch_cmd)
{
	printf("\n");

	return 0;
}
int main(int argc, char *argv[])
{
	int		ch_no;
	int		uni;
	int		sgl;
	int		pdcm;

	if (argc != 5) {
		printf("4 Argumente erforderlich:\n");
		printf("	1. Channel (0..7)\n"); 
		printf("	2. Output Code (0..1; 1: unipolar, 0: bipolar)\n"); 
		printf("	3. Single Ended / Differential Select (0..1; 1: Single Ended)\n"); 
		printf("	4. Power Down / Clock Mode Select (0..3; siehe Tabelle 7)\n");
		return -1;
	} else {
		ch_no = atoi(argv[1]);
		if (ch_no < 0 || ch_no > 7) {
			printf("1. Argument (Channel) ungÃ¼ltig (0..7)\n"); 
			return -1;
		}			
		uni = atoi(argv[2]);
		if (uni < 0 || uni > 1) {
			printf("2. Argument (Output Code) ungÃ¼ltig (0..1)\n"); 
			return -1;
		}			
		sgl = atoi(argv[3]);
		if (sgl < 0 || sgl > 1) {
			printf("3. Argument (Single Ended / Differential Select) ungültig (0..1)\n"); 
			return -1;
		}			
		pdcm = atoi(argv[4]);
		if (pdcm < 0 || pdcm > 3) {
			printf("4. Argument (Power Down / Clock Mode Select) ungültig (0..3)\n"); 
			return -1;
		}
	}			
			 




	print_lm_string_bulk(devh, ch_no, pdcm + 4*sgl + 8*uni);

//out_release:
	usb_release_interface(devh, 0);
out_close:
	usb_close(devh);
out:
	return r >= 0 ? r : -r;
}
*/
