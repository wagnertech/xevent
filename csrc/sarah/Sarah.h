#ifndef __SARAH
#define __SARAH

#include "EDCommon.h"
#include "HandledObject.h"

#include <usb.h>

class Sarah : public HandledObject{
	
public:
	Sarah(const EDCommon& edc);

	virtual int get(Event& evt, Event&);

	void setId(int);

private:
	void init();
	void measure();

	usb_dev_handle *mDeviceHandle;

	struct usb_device * find_device_with_vid_pid(
		uint16_t vendor_id,
		uint16_t product_id);

	int id; // set by getInstance(), used by get()
	int lastResult[8];
	int lastUpdate;
	bool testmode;
	int maxTime;

};

#endif
