#!/bin/bash
set -e

cd 'Debug_with_Linux GCC'
export LD_LIBRARY_PATH="."
if ! [ -e objecthandlerd ]
then
	ln -s '../../objecthandlerd/Debug_with_Linux GCC/objecthandlerd' .
fi
if ! [ -e libeventcommon.so ]
then
	ln -s '../../util/Debug_with_Linux GCC/libeventcommon.so' .
fi
if ! [ -e libhandledobject.so ]
then
	ln -s libsarah.so libhandledobject.so
fi

./objecthandlerd $*

