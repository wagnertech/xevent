//============================================================================
// Name        : sendhandlerevent.cpp
// Author      : WTG
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include "Attribute.h"
#include "DistributionRecord.h"
#include "EDCommon.h"
#include "Event.h"
#include "util.h"

#include <stdexcept>
#include <iostream>
#include <string>

#include <stdlib.h> // for atoi
#include <string.h>


using namespace std;

void usage() {
	cout << "sendhandlerevent <Tgt-Obj> <Event> [options]" << endl;
	cout << "   -D data  : send data with the event" << endl;
	cout << "   -t type  : data type (default: string)" << endl;
	cout << "   -I inst  : send data to specified instance" << endl;
	cout << "   -S       : synchronous event, wait for result" << endl;
	cout << "   -c file  : use specified config file" << endl;
	cout << "   -q name  : use specified queue name" << endl;
}

int main(int argc, char *argv[]) {
	// check parameters
	if(argc < 3) {
		usage();
		return 1;
	}

	long incarnation = 99;

	string targetObject(argv[1]);
	string event(argv[2]);

	int p_i = 3;

	string data;
	char dtype = 's';
	int instance = 0;
	bool synchron = false;
	string configFileName;
	string queueName;

	while (p_i < argc){
		if(strcmp(argv[p_i],"-c")==0){
			configFileName = argv[p_i+1];
			p_i += 2;
		}
		else if (strcmp(argv[p_i],"-q")==0){
			queueName = argv[p_i+1];
			p_i += 2;
		}
		else if (strcmp(argv[p_i],"-D")==0){
			data = argv[p_i+1];
			p_i += 2;
		}
		else if (strcmp(argv[p_i],"-t")==0){
			dtype = argv[p_i+1][0];
			p_i += 2;
		}
		else if (strcmp(argv[p_i],"-S")==0){
			synchron = true;
			p_i += 1;
		}
		else if (strcmp(argv[p_i],"-I")==0){
			instance = atoi(argv[p_i+1]);
			p_i += 2;
		}
		else {
			cerr << "invalid parameter " << argv[p_i] << endl;
			usage();
			return 1;
		}
	}

	// define buffer
	t_message qbuf;

	try {

		// get helper class
		EDCommon edCommon(configFileName, queueName);

		// open queue
		edCommon.openQueue();

		// build event
		EventStruct es;
		es.snd_class = 0;
        es.snd_inst = 0;
        es.tgt_class = edCommon.requireClassId(targetObject);
        es.tgt_inst = instance;
        es.evt_id = Event::getEventNumber(event);
        es.thread = time(0);  // number of thread
        es.synchon = synchron;
        es.data_type = dtype;
        es.length = 0;
        if (data.length() > 0) {
			switch (dtype) {
			case 's':
				es.length = data.length();
				memcpy(&es.data, data.c_str(), es.length);
				break;
			case 'f': {
				float val = util::toVal<float>(data);
				es.length = sizeof(float);
				memcpy(&es.data, (char*)&val, es.length);
				break;
			}
			case 'i': {
				int val = util::toVal<int>(data);
				es.length = sizeof(int);
				memcpy(&es.data, (char*)&val, es.length);
				break;
			}
			case 'a': {
				Attribute a(data);
				Event evt;
				a.fillDataIntoEvent(evt);
				es.length = evt.getDataLength();
				memcpy(&es.data, &evt.getData(), es.length);
				break;
			}
			default:
				throw runtime_error(string("Unknown type: ")+dtype);
		}
		}

		// find instance for new event
		DistributionRecord& distrib = edCommon.getDistributionRecord();
		int tgtHandler = distrib.getTgtHandler(es.tgt_class, es.tgt_inst);

		// send event
		qbuf.tgtIncarnation = tgtHandler;
		qbuf.sndIncarnation = incarnation;
		memcpy(&qbuf.event, &es, sizeof(EventStruct));
		edCommon.sendEvent(qbuf);
		cout << "Message sent." << endl;

		if (synchron) {

			// get event
			cout << "Waiting for response ..." << endl;
			edCommon.getEvent(qbuf, incarnation);
			Event e(qbuf.event);
			cout << e << endl;
		}
	}
	catch(std::exception& e){
		std::cerr << incarnation << " - setup error: " << e.what() << std::endl;
		return 1;
	}
	catch(...) {
		std::cerr << incarnation << " - unknown error." << std::endl;
	}
}
