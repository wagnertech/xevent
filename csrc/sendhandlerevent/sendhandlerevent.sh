#!/bin/bash
set -e

cd 'Debug_with_Linux GCC'
export LD_LIBRARY_PATH="."
if ! [ -e libeventcommon.so ]
then
	ln -s '../../util/Debug_with_Linux GCC/libeventcommon.so' .
fi

./sendhandlerevent $*

