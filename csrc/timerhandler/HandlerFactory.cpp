/*
 * HandlerFactory.cpp
 *
 *  Created on: 29.01.2014
 *      Author: gnublin
 */

#include <stdexcept>

#include "HandlerFactory.h"
#include "TimeMeasurement.h"
#include "TimerMgr.h"
#include "util.h"

void HandlerFactory::setClassIds() {
	Timer::initTimers(*edCommon);
}
HandledObject& HandlerFactory::getInstance(int classId, int inst) {
	if (this->edCommon->getClassName(classId) == "TimerMgr") return TimerMgr::getInstance();
	else if (this->edCommon->getClassName(classId) == "Timer") return Timer::getTimer(inst);
	else if (this->edCommon->getClassName(classId) == "TimeMeasurement") return TimeMeasurement::getInstance(*edCommon, inst);
#ifdef _ARMEL
	// no C++11 support
	else throw runtime_error(string("unknown class id")+util::to_string(classId));
#else
	else throw runtime_error(string("unknown class id")+std::to_string(classId));
#endif
	return TimerMgr::getInstance();
}
