/*
 * Interrupt.cpp
 *
 *  Created on: 31.08.2016
 *      Author: gnublin
 */

#include "Interrupt.h"

#include "ConfigFile.h"

#include <fstream>
#include <iostream>
#include <sstream> // stringstream

#ifdef _ARMEL
	#include "gnublin.h"
#endif

using namespace std;

Interrupt::Interrupt() {
	this->last_value = 0;
}
Interrupt::Interrupt(int id) {
	this->id = id;
	this->last_value = 0;
}

Interrupt::~Interrupt() {
}

//Interrupt::Interrupt(int id, int interval, bool single)
//Interrupt::Interrupt(const Interrupt&) = default;
//Interrupt::Interrupt(Interrupt&&);

int Interrupt::trigger(int time, Event& out){

	int val;

#ifdef _ARMEL
	string pin_str = numberToString(id);
	string filename("/sys/class/gpio/gpio" + pin_str + "/value");
#else
	// fake GPIO by reading from file
	string filename("/tmp/gpio");
	filename += to_string(this->id);
#endif

	ifstream file;
	file.open(filename.c_str());
	file >> val;
	file.close();

	// check value change
	if (val == this->last_value) return 0;
	this->last_value = val;
	out.pushData(this->message.c_str(), this->message.capacity(), 's');
	if ((val == 1) && this->up_trigger) return this->id;
	if ((val == 0) && this->down_trigger) return this->id;

	return 0;

}

std::istream& operator>>(std::istream& i, Interrupt& ir) {
	char a;
	int b;
	i >> b; // 0/1
	ir.up_trigger = b;
	i >> a; // ;
	i >> b; // 0/1
	ir.down_trigger = b;
	i >> a; // ;
	if ( !i.good()){	// this check is here, because 'message' is optional
		// read failed
		ir.id = 0;
		return i;
	}
	getline(i, ir.message); // read to end of line
	cout << "Interrupt " << ir.id << " initialized." << endl;
	return i;

}

// management stuff
void Interrupt::initInterrupt(EDCommon& edCommon){
	// get timer file name
	string timerFileName = edCommon.getConfigFileName();

	// get timer content
	ConfigFile cf(timerFileName);
	vector<string> timerLines = cf.getSectionData("interrupt");
#ifdef _ARMEL
	for (vector<string>::iterator i=timerLines.begin(); i!=timerLines.end(); i++){
		string& line = *i;
#else
	for (auto line : timerLines) {
#endif
		// convert string to stream
		stringstream ss(line);
		char a;
		int id = 0;

		// read id
		ss >> id;
		ss >> a;

		if (id > 0) {

			// a valid line
			Interrupt* i = new Interrupt(id);
			ss >> *i;
			timers.push_back(i);
		}
	}
}
