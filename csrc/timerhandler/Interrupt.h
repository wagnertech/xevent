/*
 * Interupt.h
 *
 * Obsolete: worked with gnublin board.
 *
 */

#ifndef INTERRUPT_H_
#define INTERRUPT_H_

#include "Timer.h"

#include <istream>
#include <vector>

class Interrupt: public Timer {
public:
	Interrupt();
	Interrupt(int id);
	virtual ~Interrupt();

	//Interupt(int id, int interval, bool single);
	//Interupt(const Interupt&) = default;
	//Interupt(Interupt&&);

	virtual int trigger(int time, Event&);

	friend std::istream& operator>>(std::istream& i, Interrupt& t);

	static void initInterrupt(EDCommon&);

private:
	int last_value; // last value read from GPIO
	bool up_trigger;   		// fire on 0 -> 1
	bool down_trigger;   	// fire on 1 -> 0
};



#endif /* INTERRUPT_H_ */
