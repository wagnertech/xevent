/*
 * TimeMeasurement.cpp
 *
 *  Created on: 03.08.2021
 *      Author: debian10
 */

#include "TimeMeasurement.h"

#include "EventException.h"

#include <ctime>
#include <string.h>

using namespace std;

vector<TimeMeasurement> TimeMeasurement::instances;

int TimeMeasurement::trigger(Event& in, Event& out) {
	if (in.getDataType() != 'b') throw EventException("TimeMeasurement: Type b expected.", in);
	bool b = false;
	if (in.getData() == 't') b = true;
	if (b) {
		// start measurement
		this->timestamp = time(0);
		return 1;
	}
	else {
		// stop management
		int interval = time(0) - this->timestamp;
		out.pushData((char*)&interval, sizeof(int), 'i');
		return 0;
	}
}

TimeMeasurement& TimeMeasurement::getInstance(const EDCommon&, int id) {
	if (id > TimeMeasurement::instances.size()) TimeMeasurement::instances.resize(id);
	return TimeMeasurement::instances[id-1];
}
