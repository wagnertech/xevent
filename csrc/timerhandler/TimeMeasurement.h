/*
 * TimeMeasurement.h
 *
 * Performs a time measurement between trigger events.
 *
 */

#ifndef TIMEMEASUREMENT_H_
#define TIMEMEASUREMENT_H_

#include "HandledObject.h"

#include "EDCommon.h"

#include <vector>

class TimeMeasurement: public HandledObject {
public:
	/*
	 * method used by HandlerFactory
	 * multiton pattern. Auto created instances for given id
	 */
	static TimeMeasurement& getInstance(const EDCommon&, int id);
	/*
	 * trigger method: starts/stops a measurement
	 * bool type data expected.
	 * true: start measurement  -> returns 1
	 * false: stop measurement  -> returns 0 + measured time in seconds (int)
	 */
	virtual int trigger(Event&, Event&);
#ifdef _ARMEL
	TimeMeasurement():timestamp(0){}
#endif
private:
	static std::vector<TimeMeasurement> instances;

#ifdef _ARMEL
	int timestamp;
#else
	int timestamp = 0;
#endif
};

#endif /* TIMEMEASUREMENT_H_ */
