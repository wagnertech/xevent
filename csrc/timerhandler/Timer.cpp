/*
 * TimerHandler.cpp
 *
 *  Created on: 02.04.2014
 *      Author: gnublin
 */

#include "Timer.h"

#include "ConfigFile.h"
#include "EventException.h"
#include "Interrupt.h"
#include "util.h"

#include <fstream>
#include <sstream> // stringstream
#include <iostream>
#include <stdexcept>
#include <vector>

using namespace std;

Timer::Timer() {
}
Timer::Timer(int id) {
	this->id = id;
}
Timer::Timer(int id, int interval, bool single) {
	this->id = id;
	this->last_shot = 0;
	this->intervall = interval;
	this->single = single;
	this->active = true;
	cout << "Timer " << this->id << " created." << endl;
}

/* C++11 geht nicht bei ARM
Timer::Timer(Timer&& t) :Timer(t) {
	t.id = 0;
	t.intervall = 0;
}
*/
Timer::~Timer() {
}

int Timer::trigger(int time, Event& out){
	if (!active) return 0;
	if ((time - last_shot)<intervall) return 0;
	if (single) active = false;
	last_shot = time;
	out.pushData(this->message.c_str(), this->message.capacity(), 's');
	return id;
}

int Timer::getId() {
	return this->id;
}
void Timer::setId(int id) {
	this->id = id;
}

istream& operator>>(istream& i, Timer& t){
	char a;
	int b;
	//i >> t.id;
	//i >> a; // ;
	i >> t.intervall;
	i >> a; // ;
	i >> b; // 0/1
	t.single = b;
	i >> a; // ;
	i >> b; // 0/1
	t.active = b;
	i >> a; // ;
	if ( !i.good()){	// this check is here, because 'message' is optional
		// read failed
		throw runtime_error("invalid timer format for timer: "+to_string(t.id));
		return i;
	}
	getline(i, t.message); // read to end of line
	//while (i and a != '\n') i.get(a); // read to end of line
	t.last_shot = 0;
	cout << "Timer " << t.id << " initialized." << endl;
	return i;
}

// management stuff
void Timer::initTimers(EDCommon& edCommon){
	if (timers.size() == 0){

		// get timer file name
		string timerFileName = edCommon.getConfigFileName();

		// get timer content
		ConfigFile cf(timerFileName);
		vector<string> timerLines = cf.requireSectionData("timer");
#ifdef _ARMEL
		for (vector<string>::iterator i=timerLines.begin(); i!=timerLines.end(); i++){
			string& line = *i;
#else
		for (auto line : timerLines) {
#endif
			// convert string to stream
			stringstream ss(line);
			char a;
			int id = 0;

			// read id
			ss >> id;
			ss >> a; //;
			Timer* t = new Timer(id);
			ss >> *t;

			if (id > 0) {

				// a valid line
				timers.push_back(t);
			}
		}
		Interrupt::initInterrupt(edCommon);
	}
}

Timer& Timer::getTimer(int id) {
#ifdef _ARMEL
	for (std::vector<Timer*>::iterator i=timers.begin();
			i < timers.end(); i++) {
		if((*i)->getId() == id) return **i;
	}
	throw runtime_error(string("timer not found: "));
#else
	for ( auto& t : timers ) {
		if(t->getId() == id) return *t;
	}
	throw runtime_error(string("timer not found: ")+to_string(id));
#endif
}

Timer& Timer::getNextTimer(int& i){
	if (i == (int)timers.size()-1) i=-1; // last element reached
	i++;
	return *(timers.at(i));
}

int Timer::set(Event& event) {
	// return values:
	// set intervall    -> 0
	// set single true  -> 1
	// set single false -> 2
	// set active true  -> 3
	// set active false -> 4
	int result = -1;

	// evaluation set-string
	if (event.getDataLength() == 2) {
		// string data
		char* data = &(event.getData());
		switch (data[0]) {
			case 's':
				switch (data[1]) {
					case 't': this->single = true; result = 1; break;
					case 'f': this->single = false; result = 2; break;
					default: throw EventException("invalid data value1", event);
				}
				break;
			case 'a':
				switch (data[1]) {
					case 't':
						this->active = true;
						this->last_shot = time(0);
						result = 3;
						break;
					case 'f': this->active = false; result = 4; break;
					default: throw EventException("invalid data value2", event);
				}
				break;
			default: throw EventException("invalid data value3", event);
		}
	}
	else if (event.getDataLength() == sizeof(int)) {
		// set timer intervall
		this->intervall = *((int*)&(event.getData()));
		result = 0;
	}
	else throw EventException("invalid data length", event);
	return result;
}

vector<Timer*> Timer::timers;

