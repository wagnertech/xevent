/*
 * Timer.h
 *
 * Timer objekt implementing
 * - single shot timers
 * - periodic timers
 * Timers have an active flag. If set to "not active", a timer instance does nothing.
 * Timers are defined in the [Timer] section of the configuration file:
 * <timerid>;<interval in s>;0|1 single flag;0|1 active flag;<text sent, if timer fires>
 */

#ifndef Timer_H_
#define Timer_H_

#include "EDCommon.h"
#include "HandledObject.h"

#include <istream>
#include <vector>
#include <memory>

class Timer: public HandledObject {
public:
	Timer();
	Timer(int id);
	Timer(int id, int interval, bool single);
	Timer(const Timer&) {}
	// C++11 geht nicht bei ARM
	//Timer(Timer&&);
	virtual ~Timer();

	virtual int trigger(int time, Event&);

	int getId();
	void setId(int);

	friend std::istream& operator>>(std::istream& i, Timer& t);

	// management function
	static Timer& getTimer(int i);
	static Timer& getNextTimer(int& i);
	static void initTimers(EDCommon&);

	/*
	 * set method.
	 * If string data is supplied:
	 * st: sets single shot timer
	 * sf: sets periodic timer
	 * at: sets active true
	 * af: sets timer inactive
	 * If integer data is supplied:
	 * Value is set as timer interval
	 */
	virtual int set(Event&);

private:
	int last_shot; // unix time stamp
	int intervall; // in sec
	bool single;   // to be set inactive after shot
	bool active;   // fires, if time is over

protected:
	int id;        // this id is returned at trigger()
	std::string message;// message sent with event

	// management data
	static std::vector<Timer*> timers;


};

#endif /* Timer_H_ */
