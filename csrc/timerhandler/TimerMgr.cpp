/*
 * TimerMgr.cpp
 *
 *  Created on: 02.04.2014
 *      Author: gnublin
 */

#include <iostream>
#include <ctime>
//#include <memory>
#include "TimerMgr.h"

using namespace std;

TimerMgr* TimerMgr::theInstance = 0;

TimerMgr::TimerMgr()
: lastTimerIdx(0) {}

TimerMgr& TimerMgr::getInstance() {
	if (!theInstance) theInstance = new TimerMgr();
	return *theInstance;
}

int TimerMgr::trigger(Event& event, Event& out){
	Timer& currTimer = Timer::getNextTimer(this->lastTimerIdx);
	int ret = currTimer.trigger(time(0), out);
	return ret;
}
