/*
 * TimerMgr.h
 *
 *  Created on: 02.04.2014
 *      Author: gnublin
 */

#ifndef TIMERMGR_H_
#define TIMERMGR_H_

#include <vector>
#include <memory>

#include <HandledObject.h>
#include "Timer.h"

class TimerMgr: public HandledObject {
public:
	static TimerMgr& getInstance();
	// virtual ~TimerMgr() = default; C++11 geht nicht auf ARM

	virtual int trigger(Event&, Event&);

private:
	// TimerMgr() = default; C++11 geht nicht auf ARM
	TimerMgr();
	int lastTimerIdx; // {0}; C++11 geht nicht auf ARM

	static TimerMgr* theInstance;
};


#endif /* TIMERMGR_H_ */
