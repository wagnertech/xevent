/*
 * Attribute.cpp
 *
 *  Created on: 09.11.2021
 *      Author: debian10
 */

#include "Attribute.h"
#include "EventException.h"
#include "util.h"

#include <memory.h> // memcpy
#include <stdexcept>

using namespace std;

Attribute::Attribute(std::string att_string) {
	char att_type = 'X'; // undefined value
	string att_value_string;
	bool ret = util::split(att_string, ':', this->att_name, att_type, att_value_string);
	if (!ret) throw EventException(string("Attribute: cannot convert attribute string"+att_string).c_str());
	switch (att_type) {
	case 'f': { // for localization of variable float_data
		float data = util::toVal<float>(att_value_string);
		this->att_event.pushData((const char*)&data, sizeof(float), 'f');
	} break;
	case 'i': { // for localization of variable int_data
		int data = util::toVal<int>(att_value_string);
		this->att_event.pushData((const char*)&data, sizeof(int), 'i');
	} break;
	case 'b': { // for localization of variable bool_data
		bool data = util::toVal<bool>(att_value_string);
		this->att_event.pushData((const char*)&data, sizeof(bool), 'b');
	} break;
	case 's':
		this->att_event.pushData(att_value_string.c_str(), att_value_string.length(), 's');
		break;
	default:
		throw runtime_error(string("Attribute: unknown attribute type: ")+att_type);
	}
}

Attribute::Attribute(int len, const char *att_ptr) {
	char bytes[MAX_DATA_SIZE];
	char nam_len = att_ptr[0];
	char att_type = att_ptr[nam_len+1];
	memcpy(bytes, att_ptr+1, nam_len);
	// null terminate string
	bytes[nam_len] = 0;
	this->att_name = string(bytes);
	this->att_event.pushData(att_ptr+nam_len+2, len-nam_len-1, att_type);
}

void Attribute::fillDataIntoEvent(Event& evt) {
	char bytes[MAX_DATA_SIZE];
	bytes[0] = this->att_name.length();
	memcpy(&bytes[1], this->att_name.c_str(), bytes[0]);
	bytes[bytes[0]+1] = this->att_event.getDataType();
	memcpy(&bytes[bytes[0]+2], &this->att_event.getData(), this->att_event.getDataLength());
	evt.pushData(bytes, bytes[0]+2+this->att_event.getDataLength(), 'a');
}

void Attribute::fillAttributeValue(const Event& evt) {
	this->att_event = evt;
}

std::string Attribute::getName() {
	return this->att_name;
}

const Event& Attribute::getEvent() {
	return this->att_event;
}

std::ostream& operator<<(std::ostream& os, const Attribute& a){
	os << a.att_name << ":" << a.att_event.getDataType() << ":";
	const char* data_ptr = &(a.att_event.getData());
	switch (a.att_event.getDataType()) {
	case 'f': { // for localization of variable float_data
		float float_data;
		memcpy(&float_data, data_ptr, sizeof(float));
		os << float_data;
	} break;
	case 'i': { // for localization of variable int_data
		int int_data;
		memcpy(&int_data, data_ptr, sizeof(int));
		os << int_data;
	} break;
	case 'b': { // for localization of variable bool_data
		bool bool_data;
		memcpy(&bool_data, data_ptr, sizeof(bool));
		os << bool_data;
	} break;
	default:
		for(int i = 0; i<a.att_event.getDataLength(); i++) os << data_ptr[i];
	}
	return os;
}
