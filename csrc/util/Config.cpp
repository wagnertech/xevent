/*
 * Config.cpp
 *
 *  Created on: 11.09.2015
 *      Author: gnublin
 */

#include "Config.h"

Config* Config::theInstance = NULL;

Config::Config() {
}

Config::~Config() {
}

void Config::add(std::string& key,std::string& value){
	this->configs[key] = value;
}

std::string Config::get(std::string& key){
	std::map<std::string,std::string>::iterator iter = this->configs.find(key);
	if (iter!=this->configs.end())
		return iter->second;
	return std::string();
}

Config* Config::getInstance(){
	if(! Config::theInstance) Config::theInstance = new Config();
	return Config::theInstance;
}
