/*
 * Config.h
 *
 *  Created on: 11.09.2015
 *      Author: gnublin
 */
// TODO: Wird wohl nicht mehr gebraucht
#ifndef CONFIG_H_
#define CONFIG_H_

#include <map>
#include <string>

class Config {
private:
	Config();

	std::map<std::string,std::string> configs;

	static Config* theInstance;

public:
	virtual ~Config();

	void add(std::string&,std::string&);
	std::string get(std::string&);

	static Config* getInstance();
};

#endif /* CONFIG_H_ */

