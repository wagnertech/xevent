/*
 * ConfigFile.cpp
 *
 *  Created on: 21.11.2016
 *      Author: gnublin
 */

#include "ConfigFile.h"

#include <fstream>
#include <stdexcept>

using namespace std;

vector<string> ConfigFile::getSectionData(string section) {
	vector<string> v;
	ifstream file(this->fileName.c_str()); // .c_str is needed for ARM
	if (!(file.good())) throw runtime_error(string("cannot open file ")+this->fileName);
	bool sectionRunning = true;
	while (file && sectionRunning) {
		string line;
		getline(file, line);
		if (line.size() > 0 && line[0] != '#') {

			// no comment line

			// search secton start
			if (line == string("[")+section+"]") {

				// read section data
				while (file && sectionRunning) {
					getline(file, line);
					if (line.size() > 0 && line[0] != '#') {
						if (line[0] == '[') sectionRunning = false;
						else v.push_back(line);
					}
				}
			}
		}
	}
	file.close();
	return v;
}

vector<string> ConfigFile::requireSectionData(string section) {
	vector<string> r = this->getSectionData(section);
	if (r.size() == 0) throw runtime_error("Section ["+section+"] empty/not found");
	return r;
}
