/*
 * DistributionRecord.cpp
 *
 *  Created on: 11.03.2014
 *      Author: gnublin
 */

//#include <stdio.h>
#include "DistributionRecord.h"

#include "ConfigFile.h"
#include "EventException.h"
#include "HandlerRecord.h"
#include "util.h"

#include <sstream>
#include <stdexcept>

DistributionRecord::DistributionRecord(string filename, vector<string>& class_list) {
	ConfigFile cf(filename);
	vector<string> distLines = cf.requireSectionData("distribution");
	size_t objectClass = 0;
#ifdef _ARMEL
	for (vector<string>::iterator i=distLines.begin(); i!=distLines.end(); i++){
		string& line = *i;
#else
	for (auto line : distLines) {
#endif
		line +=";";
		istringstream iss;
		iss.str(line);

		// data
		string className;
		size_t inst;
		int handler;
		util::split(iss, ';', className, inst, handler);
		if (iss.fail()) throw runtime_error("Parse error: "+line);

		// fill data structure
		this->distrib.resize(objectClass+1);
		if (this->distrib[objectClass].size() < inst+1) this->distrib[objectClass].resize(inst+1);
		this->distrib[objectClass][inst] = handler;
		class_list.push_back(className);
		objectClass++;
	}
}

int DistributionRecord::getTgtHandler(int tgtClass, int tgtInst) {
	if (tgtInst > 0) {
		// try event specific
		try {
			int i = this->distrib.at(tgtClass).at(tgtInst);
			if (i > 0) return i;
		}
		catch (exception& e) {
			// continue
		}
	}
	// set common distribution
	tgtInst = 0;
	try {
		int i = this->distrib.at(tgtClass).at(tgtInst);
		if (i == 0) throw EventException("DistributionRecord: Could not find target handler.");
		return i;
	}
	catch (exception& e) {
		throw EventException("DistributionRecord: Could not find target handler.");
	}
	return -1;
}
