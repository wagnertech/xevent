/*
 * EDCommon.cpp
 *
 *  Created on: 30.09.2014
 *      Author: gnublin
 */

#include "EDCommon.h"

#include "ConfigFile.h"
#include "DistributionRecord.h"
#include "EventException.h"
#include "HandlerRecord.h"

#include <algorithm>
#include <errno.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdexcept>
#include <sstream>
#include <string.h> // strerror

EDCommon::EDCommon(string& configFileName, string& queueName)
: distributionRecord(NULL)
, handlerRecord(NULL)
, queueId(-1)
, configFileName(configFileName) {

	// supply parameter defaults
	if (configFileName.empty()) {
		configFileName = "/etc/xevent-handler.conf"; // can be supplied as runtime parameter
		this->configFileName = configFileName;
	}
	if (queueName.empty()) this->queueName = "/tmp"; // constant as long no other site is used
	else this->queueName = queueName;

	// read configuration data
	ConfigFile cf(configFileName);
	this->distributionRecord = new DistributionRecord(configFileName, this->class_list);
	this->handlerRecord = new HandlerRecord(configFileName, *this);
}

EDCommon::~EDCommon() {
	delete this->distributionRecord;
	delete this->handlerRecord;
}

DistributionRecord& EDCommon::getDistributionRecord(){
	return *this->distributionRecord;
}

HandlerRecord& EDCommon::getHandlerRecord() {
	return *this->handlerRecord;
}

string EDCommon::getConfigFileName() const {
	return this->configFileName;
}

void EDCommon::openQueue(){

	// get queue key
	key_t key;
	if ((key = ftok(this->queueName.c_str(), 1)) == -1){
		stringstream errstr;
		errstr << "fetch queue key failed: " << strerror(errno) << endl;
		throw runtime_error(errstr.str());
	}

	// open queue
	if ((this->queueId = msgget( key, IPC_CREAT | 0666 )) == -1){
		stringstream errstr;
		errstr << "open queue failed: " << strerror(errno) << std::endl;
		throw runtime_error(errstr.str());
	}
}

void EDCommon::getEvent(t_message& qbuf, long incarnation){
	// check, if queue open
	if (this->queueId == -1) throw runtime_error("queue not open!\n");
	int size = msgrcv(this->queueId, &qbuf, event_size, incarnation, 0);
	if (size == -1){
		stringstream errstr;
		errstr << "read failed: " << strerror(errno) << std::endl;
		throw runtime_error(errstr.str());
	}

}

void EDCommon::sendEvent(t_message& qbuf){
	// check, if queue open
	if (this->queueId == -1) throw runtime_error("queue not open!\n");

    if (msgsnd(this->queueId, &qbuf, event_size, 0) ==-1) {
    	throw EventException("msgsnd returned -1");
    }
}

int EDCommon::getClassId(std::string name) const {
	vector<string>::const_iterator i = find(this->class_list.begin(), this->class_list.end(), name);
	if (i == this->class_list.end()) return -1;
	return i - this->class_list.begin();
}
int EDCommon::requireClassId(std::string name) const {
	int i = this->getClassId(name);
	if (i == -1) throw runtime_error(string("EDCommon: class not found: "+name));
	return i;
}

std::string EDCommon::getClassName(int i) const {
	return this->class_list.at(i);
}

const t_class_list& EDCommon::getClassList() const{
	return this->class_list;
}

