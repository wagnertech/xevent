/*
 * EDCommon.h
 *
 *  Created on: 30.09.2014
 *      Author: gnublin
 */

#ifndef EDCOMMON_H_
#define EDCOMMON_H_

#include "Event.h"

#include <string>
#include <vector>

class DistributionRecord;
class HandlerRecord;

typedef std::vector<std::string> t_class_list;

// utility class for implementing user libraries
class EDCommon {
public:
	EDCommon(std::string& configFileName, std::string& queueName);
	virtual ~EDCommon();

	// returns the DistributionRecord. See DistributionRecord.h
	DistributionRecord& getDistributionRecord();

	// returns the HandlerRecord. See HandlerRecord.h
	HandlerRecord& getHandlerRecord();

	// opens the event queue. Not to be used in the user library
	void openQueue();

	// fetches a request from event queue. Not to be used in the user library
	void getEvent(t_message&, long incarnation);

	// Sends a event via event queue. Not to be used in the user library
	void sendEvent(t_message&);

	// returns the configuration file name. To be used in the constructor of ConfigFile
	std::string getConfigFileName() const;

	/*
		returns the class id of a class identified by name
		Params: className
		Returns the class id.
	      -1 means "not found"
	*/
	int getClassId(std::string className) const;
	// like getClassId, but throws exception if not found
	int requireClassId(std::string) const;
	// throws exception if out of range
	/*
		returns the class name of a class identified by id
		Params: classId
		Returns the class name.
	      Throws an exception, if not found
	*/
	std::string getClassName(int classId) const;

	// returns the complete class list
	const t_class_list& getClassList() const;

private:
	DistributionRecord* distributionRecord;
	HandlerRecord* handlerRecord;
	string queueName;
	int queueId;
	string configFileName;
	t_class_list class_list;
};

#endif /* EDCOMMON_H_ */
