/*
 * Event.cpp
 *
 *  Created on: 27.12.2013
 *      Author: gnublin
 */

#include "Attribute.h"
#include "Event.h"
#include "EventException.h"

#include <memory.h> // memcpy
#include <iostream>
#include <ostream>
#include <stdexcept>
	using std::runtime_error;

/*
int Event::EVT_RESPONSE = 1;
int Event::EVT_CREATE = 2;
int Event::EVT_GET = 3;
int Event::EVT_SET = 4;
int Event::EVT_DELETE = 5;
int Event::EVT_TRIGGER = 6;
int Event::EVT_ANZAHL = 7;
*/
#ifdef _ARMEL

#include "util.h"
	using namespace util;

#endif

Event::Event()
{
	this->data.length = 0;
}
Event::Event(EventStruct& data)
{
	this->data = data;
}

Event::~Event()
{
}

void Event::pushData(const char* dataAddress, long dataLength, char dataType) {
	if (dataLength < MAX_DATA_SIZE) {
		memcpy(this->data.data, dataAddress, dataLength);
		this->data.length = dataLength;
		this->data.data_type = dataType;
	}
	else throw EventException("Data size exeeded");
}

int Event::getTgtInst() const {
	return data.tgt_inst;
}
int Event::getTgtClass() const {
	return data.tgt_class;
}
bool Event::isSynchron(){
	return data.synchon;
}
int Event::getSndClass(){
	return data.snd_class;
}
int Event::getSndInst(){
	return data.snd_inst;
}
int Event::getThread(){
	return data.thread;
}
EventStruct& Event::getEventStruct() {
	return this->data;
}
int Event::getEventId() {
	return this->data.evt_id;
}
int Event::getDataLength() const {
	return this->data.length;
}
char Event::getDataType() const {
	return this->data.data_type;
}
char& Event::getData() {
	return *this->data.data;
}
const char& Event::getData() const {
	return *this->data.data;
}

int Event::getEventNumber(std::string& name) {
	if (name.compare("RESPONSE") == 0) return EVT_RESPONSE;
	else if (name.compare("CREATE") == 0) return EVT_CREATE;
	else if (name.compare("SET") == 0) return EVT_SET;
	else if (name.compare("GET") == 0) return EVT_GET;
	else if (name.compare("DELETE") == 0) return EVT_DELETE;
	else if (name.compare("TRIGGER") == 0) return EVT_TRIGGER;
	else throw EventException((std::string("unknown event")+name).c_str());
}

string Event::getEventName() const {
	switch (this->data.evt_id) {
	case EVT_RESPONSE: return string("RESPONSE");
		break;
	case EVT_CREATE: return string("CREATE");
		break;
	case EVT_SET: return string("SET");
		break;
	case EVT_DELETE: return string("DETETE");
		break;
	case EVT_GET: return string("GET");
		break;
	case EVT_TRIGGER: return string("TRIGGER");
		break;
#ifdef _ARMEL
	default: throw runtime_error(string("invalid event id:")+util::to_string(this->data.evt_id));
#else
	default: throw runtime_error(string("invalid event id:")+to_string(this->data.evt_id));
#endif
	}
}

void Event::setTgtClass(int c) {
	this->data.tgt_class = c;
}
void Event::setTgtInst(int i) {
	this->data.tgt_inst = i;
}
void Event::setSndClass(int c) {
	this->data.snd_class = c;
}
void Event::setSndInst(int i) {
	this->data.snd_inst = i;
}
void Event::setEvtId(int e) {
	this->data.evt_id = e;
}
void Event::setThread(long int t) {
	this->data.thread = t;
}
void Event::setSynchon(bool s) {
	this->data.synchon = s;
}

ostream& operator<<(ostream& os, const Event& e){
	os << "Type: " << e.getEventName()
	   << "; TgtCls: " << e.getTgtClass()
	   << "; TgtInst: " << e.getTgtInst()
	   << "; data_length: " << e.getDataLength();
	if (e.getDataLength() > 0) {
		const char* data_ptr = &(e.getData());
		switch (e.getDataType()) {
		case 'f': { // for localization of variable float_data
			float float_data;
			memcpy(&float_data, data_ptr, sizeof(float));
			os << "; float: " << float_data;
		} break;
		case 'i': { // for localization of variable int_data
			int int_data;
			memcpy(&int_data, data_ptr, sizeof(int));
			os << "; int: " << int_data;
		} break;
		case 'b':
			os << "; bool: " << *data_ptr;
		break;
		case 'a': { // for localization of variable att
			Attribute att(e.getDataLength(), data_ptr);
			os << "; Attribute: " << att;
		} break;
		default:
			os << "; data: ";
			for (int i=0; i<e.getDataLength(); i++) os << data_ptr[i];
		}
	}
	return os;
}
