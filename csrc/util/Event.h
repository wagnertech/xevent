/*
 * Event.h
 *
 *  Created on: 27.12.2013
 *      Author: gnublin
 */

#ifndef EVENT_H_
#define EVENT_H_

#define MAX_DATA_SIZE 300
#define MAX_SEND_SIZE MAX_DATA_SIZE+4

#include <string>

using namespace std;

typedef struct {
        int snd_class;
        int snd_inst;
        int tgt_class;
        int tgt_inst;
        int evt_id;
        long thread;  // number of thread
        bool synchon;
        int length;
        char data_type; // b,i,l,f,d,s,o(bject),a(ttribute)
        char data[MAX_DATA_SIZE];
} EventStruct;

typedef struct message {
	long tgtIncarnation;
	long sndIncarnation;
	EventStruct event;
} t_message;

const int event_size = sizeof(t_message) - sizeof(long);

class Event {
public:
	// generic events
	static const int EVT_RESPONSE = 1;
	static const int EVT_CREATE = 2;
	static const int EVT_GET = 3;
	static const int EVT_SET = 4;
	static const int EVT_DELETE = 5;
	static const int EVT_TRIGGER = 6;
	static const int EVT_ANZAHL = 7;

public:
	Event(EventStruct& data);
	Event();
	virtual ~Event();

	int getSndClass();
	int getSndInst();
	int getTgtClass() const;
	int getTgtInst() const;
	int getEventId();
	string getEventName() const;
	int getThread();
	bool isSynchron();
	int getDataLength() const;
	char getDataType() const;
	char& getData();
	const char& getData() const;
	void pushData(const char* dataAdress, long dataLength, char dataType);
	EventStruct& getEventStruct();

	void setTgtClass(int);
	void setTgtInst(int);
	void setSndClass(int);
	void setSndInst(int);
	void setEvtId(int);
	void setThread(long int);
	void setSynchon(bool);

	static int getEventNumber(std::string&);

private:

	EventStruct data;
};

ostream& operator<<(ostream&, const Event&);

#endif /* EVENT_H_ */
