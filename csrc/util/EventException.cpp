/*
 * EventException.cpp
 *
 *  Created on: 25.06.2013
 *      Author: gnublin
 */

#include <string>
#include <sstream>
#include <stdexcept>
#include "EventException.h"
#include "util.h"

EventException::EventException(const char* msg)
: runtime_error (msg),
  event(NULL)
{
	//this->msg = msg;
}


EventException::EventException(const char* msg, int r)
#ifdef _ARMEL
: runtime_error(string(msg)+" : "+util::to_string(r)) {}
#else
: runtime_error(string(msg)+" : "+std::to_string(r)) {}
#endif


EventException::EventException(const char* msg, const Event& event)
: runtime_error(msg) {
	this->event = &event;
}

bool EventException::hasEvent() {
	return (event != NULL);
}

const Event& EventException::getEvent() {
	if (!event) throw runtime_error("EventException: no event present.");
	return *event;
}
