/*
 * HandlerRecord.cpp
 *
 *  Created on: 11.03.2014
 *      Author: gnublin
 */
#include "HandlerRecord.h"

#include "Attribute.h"
#include "ConfigFile.h"
#include "util.h"

#include <string.h>
#include <sstream>
#include <stdexcept>

HandlerRecord::HandlerRecord(std::string filename, const EDCommon& ed_common) {
	// initialize event dictionary
	size_t class_cnt = ed_common.getClassList().size();
	this->events.resize(class_cnt);
	for (size_t i = 0; i<class_cnt; i++) {
		this->events[i].resize(Event::EVT_ANZAHL);
	}

	ConfigFile cf(filename);
	vector<string> eventLines = cf.requireSectionData("event");
#ifdef _ARMEL
	for (vector<string>::iterator i=eventLines.begin(); i!=eventLines.end(); i++){
		string& line = *i;
#else
	for (auto line : eventLines) {
#endif

		// indexes
		stringstream iss(line);
		string className;
		string eventName;
		int rc;
		util::split(iss, ';', className, eventName, rc);
		size_t senderClass = ed_common.requireClassId(className);
		int inEvent = Event::getEventNumber(eventName);

		// out event struct
		struct outevent_t out_evt_struct;
		bool ret = util::split(iss, ';', eventName, out_evt_struct.def_data, className); //, out_evt_struct.tinst, out_evt_struct.synchron);
		if (!ret) throw runtime_error("Parse error: "+line);
		if (className.size() == 1) {
			// new feature: this is the data type
			out_evt_struct.data_type = className[0];
			ret = util::split(iss, ';', className, out_evt_struct.tinst, out_evt_struct.synchron);
		}
		else {
			out_evt_struct.data_type = 's'; // default
			ret = util::split(iss, ';', out_evt_struct.tinst, out_evt_struct.synchron);
		}
		if (!ret) throw runtime_error("Parse error: "+line);
		out_evt_struct.ouevent = Event::getEventNumber(eventName);
		out_evt_struct.tclass = ed_common.requireClassId(className);

		// add into event array
		this->events[senderClass][inEvent][rc].push_back(out_evt_struct);
	}
}

vector<Event> HandlerRecord::getEvents(Event& oldEvent, int opResult) {

	vector<Event> out_events;

	// get event map
#ifdef _ARMEL
	// no C++11 support
	map<int,vector<outevent_t> > em = events.at(oldEvent.getTgtClass()).at(oldEvent.getEventId());
	map<int, vector<outevent_t> >::iterator eventPartIt = em.find(opResult);
#else

	auto em = events.at(oldEvent.getTgtClass()).at(oldEvent.getEventId());

	// check, if a subsequent event is defined
	auto eventPartIt = em.find(opResult);
#endif
	if (eventPartIt == em.end()) return out_events;

#ifdef _ARMEL
	// no C++11 support
	for(vector<outevent_t>::iterator it=eventPartIt->second.begin(); it<eventPartIt->second.end(); it++){
		outevent_t elem = *it;
#else
	for(outevent_t& elem : eventPartIt->second) {
#endif

		EventStruct event_s;
		event_s.evt_id = elem.ouevent;
		event_s.length = elem.def_data.length();
		event_s.snd_class = oldEvent.getTgtClass();
		event_s.snd_inst = oldEvent.getTgtInst();
		event_s.synchon = elem.synchron;
		event_s.tgt_class = elem.tclass;
		event_s.tgt_inst = elem.tinst;
		event_s.thread = oldEvent.getThread();
		// convert types
		if (elem.data_type == 'i') {
			int tmp = util::toVal<int>(elem.def_data);
			event_s.length = sizeof(int);
			memcpy(event_s.data,(char*)&tmp,sizeof(int));
		}
		else if (elem.data_type == 'f') {
			float tmp = util::toVal<float>(elem.def_data);
			event_s.length = sizeof(float);
			memcpy(event_s.data,(char*)&tmp,sizeof(float));
		}
		else if (elem.data_type == 's') {
			strncpy(event_s.data,elem.def_data.c_str(),elem.def_data.length());
		}
		else if (elem.data_type == 'b') {
			*event_s.data = elem.def_data[0];
			event_s.length = 1;
		}
		else if (elem.data_type == 'a') {
			; // attribute is processed later
		}
		else throw runtime_error(string("HandlerRecord: type not implemented: ")+elem.data_type);
		event_s.data_type = elem.data_type;

		Event e(event_s);

		if (elem.data_type == 'a') {
			Attribute a(elem.def_data);
			a.fillDataIntoEvent(e);
		}

		out_events.push_back(e);
	}
	return out_events;
}

