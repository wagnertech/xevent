/*
 * ActiveLeafUpdateException.cpp
 *
 *  Created on: 06.09.2016
 *      Author: gnublin
 */

#include "ActiveLeafUpdateException.h"

namespace xfuzz {

int xfuzz::ActiveLeafUpdateException::getActiveValueIdx() {
	return this->activeValueIdx;
}
}
