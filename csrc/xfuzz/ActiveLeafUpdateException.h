/*
 * EventException.h
 *
 *  Created on: 25.06.2013
 *      Author: gnublin
 */

#ifndef ActiveLeafUpdateException_H_
#define ActiveLeafUpdateException_H_

#include "ActiveValue.h"

/* This exception is used to indicate an necessary update
 * of an ActiveLeaf.
 */
namespace xfuzz {

class ActiveLeafUpdateException {
public:
	ActiveLeafUpdateException(int av_idx): activeValueIdx(av_idx) {}

	int getActiveValueIdx();;

private:
	int activeValueIdx;
};
} // namespace xfuzz

#endif /* ActiveLeafUpdateException_H_ */
