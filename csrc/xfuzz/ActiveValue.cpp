/*
 * ActiveValue.cpp
 *
 *  Created on: 10.02.2022
 *      Author: debian10
 */

#include "ActiveValue.h"

#include "ActiveLeafUpdateException.h"

float xfuzz::ActiveValue::getValue() {
	if (this->utime == 0 || (time(0) - this->last_update) < this->utime) return this->vallue;
	throw ActiveLeafUpdateException(this->my_no);
}

void xfuzz::ActiveValue::setValue(float value) {
	this->vallue = value;
	this->last_update = time(0);
}

// dummy implementation. Active value deals with float.
xfuzz::FuzzyNumber xfuzz::ActiveValue::get(std::stringstream* ssp) {
	return xfuzz::FuzzyNumber(0);
}
std::string xfuzz::ActiveValue::getName() {
	return this->name;
}
