/*
 * ActiveValue.h
 *
 *  Created on: 10.02.2022
 *      Author: debian10
 */

#ifndef ACTIVEVALUE_H_
#define ACTIVEVALUE_H_

#include <string>

#include "FuzzyElem.h"
namespace xfuzz {

class ActiveValue : public FuzzyElem, public HandledObject {
	/* Idee: Wenn die deleyTime abgelaufen ist, wirft die get eine
	 * 		Exception mit this. Der RootNode (die Klasse leitet sich von
	 * 		FuzzyNode ab) muss die Exception fangen, this der Factory als
	 * 		Sender/Empfänger des folgenden sync GET mitteilen und den RC aus this
	 * 		zurückgeben.
	 *
	 * 		Im Event-File muss für diesen RC der Wertlieferant mit einem sync GET
	 * 		angesprochen werden. Der RESPONSE-Event geht dann direkt an this.
	 * 		response() führt zu einer Aktualisierug der Daten. Das Regelwerk macht
	 * 		dann weiter.
	 *
	 * 		Dieser Mechanismus wird mit einem hinterlegten RC=0 ausgeschaltet.
	 */

private:
	//int av_idx;
	// time, how long this value is valid. If time over an ActiveLeafUpdateException is thrown
	// utime=0 means: never throws an exception
	int utime;
	int last_update = 0;
	// value
	float vallue;
	std::string name;
public:
	ActiveValue(int my_no, std::string name, int utime=0)
		:FuzzyElem(my_no), utime(utime), name(name) {}
	float getValue();
	void setValue(float);
	std::string getName();
	virtual xfuzz::FuzzyNumber get(std::stringstream* ssp = nullptr);
};

} /* namespace xfuzz */

#endif /* ACTIVEVALUE_H_ */
