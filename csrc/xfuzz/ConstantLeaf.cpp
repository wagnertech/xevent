/*
 * ConstantLeaf.cpp
 *
 *  Created on: 20.07.2021
 *      Author: debian10
 */

#include "ConstantLeaf.h"

#include <sstream>

xfuzz::FuzzyNumber xfuzz::ConstantLeaf::get(std::stringstream* ssp) {
	if (ssp) *ssp <<"<CL id=\"" <<this->my_no <<"\">" <<this->f <<"</CL>";
	return this->f;
}
