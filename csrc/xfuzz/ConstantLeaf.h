/*
 * ConstantLeaf.h
 *
 *  Created on: 20.07.2021
 *      Author: debian10
 */

#ifndef CONSTANTLEAF_H_
#define CONSTANTLEAF_H_

#include "FuzzyElem.h"

namespace xfuzz {

class ConstantLeaf: public FuzzyElem {
public:
	ConstantLeaf(int my_no, int p)
		: FuzzyElem(my_no) {this->f = p;}
	virtual FuzzyNumber get(std::stringstream* ssp = nullptr);
};


} /* namespace xfuzz */

#endif /* CONSTANTLEAF_H_ */
