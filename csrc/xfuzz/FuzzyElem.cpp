/*
 * FuzzyElem.cpp
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#include "FuzzyElem.h"

#include <stdexcept>

using namespace std;

char xfuzz::toChar(FuzzyOperator op) {
	switch (op) {
	case PLUS:
		return 'P';
	case MINUS:
		return 'M';
	case TIMES:
		return 'T';
	case DIV:
		return 'D';
	case AND:
		return 'A';
	case OR:
		return 'O';
	case NOT:
		return 'N';
	default:
		throw runtime_error("FuzzyElem: Invalid FozzyOperator");
	}
}
