/*
 * FuzzyElem.h
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#ifndef FUZZYELEM_H_
#define FUZZYELEM_H_

#include "FuzzyNumber.h"
#include "HandledObject.h"

#include <map>

namespace xfuzz {

class FuzzyElem;
typedef enum fo {PLUS,MINUS,TIMES,DIV,AND,OR,NOT} FuzzyOperator;
typedef std::map<int,FuzzyElem*> t_rule_elems;

class FuzzyElem {
public:
	FuzzyElem(int my_no, t_rule_elems* re = nullptr)
		: rule_elems(re), my_no(my_no) {}

	virtual FuzzyNumber get(std::stringstream* ssp = nullptr) = 0;

protected:
	FuzzyNumber f{0};
	t_rule_elems* rule_elems;
	int my_no;
};

char toChar(FuzzyOperator op);

} /* namespace xfuzz */

#endif /* FUZZYELEM_H_ */
