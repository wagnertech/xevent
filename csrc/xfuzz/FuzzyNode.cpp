/*
 * FuzzyNode.cpp
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#include "FuzzyNode.h"

#include <sstream>
#include <stdexcept>

using namespace xfuzz;

FuzzyNumber FuzzyNode::get(std::stringstream* ssp) {
	// recalc fuzzy value
	if (ssp) *ssp <<"<" <<toChar(this->o) <<" id=\"" <<this->my_no <<"\">";
	FuzzyNumber f1 = this->rule_elems->at(e1)->get(ssp);
	FuzzyNumber f2{0};
	if (this->rule_elems->count(e2)) f2 = this->rule_elems->at(e2)->get(ssp);
	switch (this->o) {
	case PLUS :	this->f = f1 + f2; break;
	case MINUS: this->f = f1 - f2; break;
	case TIMES: this->f = f1 * f2; break;
	case DIV  : this->f = f1 / f2; break;
	case AND  : this->f = f1 && f2; break;
	case OR   : this->f = f1 || f2; break;
	case NOT  : this->f = !f1; break;
	default: throw runtime_error("Invalid Operator");
	}
	if (ssp) *ssp << this->f <<"</" <<toChar(this->o) <<">";
	return this->f;
}

xfuzz::FuzzyNode::FuzzyNode(int my_no, t_rule_elems* re, std::string o, int e1, int e2)
	:FuzzyElem(my_no, re), e1(e1), e2(e2){
	if (o == "PLUS") this->o = PLUS;
	else if (o == "MINUS") this->o = MINUS;
	else if (o == "TIMES") this->o = TIMES;
	else if (o == "DIV")   this->o = DIV;
	else if (o == "AND")   this->o = AND;
	else if (o == "OR")    this->o = OR;
	else if (o == "NOT")   this->o = NOT;
	else throw runtime_error("unknown operator: "+o);
}
