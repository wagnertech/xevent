/*
 * FuzzyNode.h
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#ifndef FUZZYNODE_H_
#define FUZZYNODE_H_

#include "FuzzyElem.h"

namespace xfuzz {

class FuzzyNode: public FuzzyElem {
public:
	FuzzyNode(int my_no, t_rule_elems* re, FuzzyOperator o, int e1, int e2)
		:FuzzyElem(my_no, re), o(o), e1(e1), e2(e2) {}
	FuzzyNode(int my_no, t_rule_elems* re, std::string o, int e1, int e2);

	virtual FuzzyNumber get(std::stringstream* ssp = nullptr);

private:
	FuzzyOperator o;
	int e1;
	int e2;
};

} /* namespace xfuzz */

#endif /* FUZZYNODE_H_ */
