/*
 * FuzzyNumber.cpp
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#include "FuzzyNumber.h"

#include <stdexcept>

namespace xfuzz {

using namespace std;

FuzzyNumber::FuzzyNumber(int intrep) {
	if (intrep<0 || intrep>1000) throw runtime_error("invalid integer fuzzy range");
	this->p = intrep;
}
FuzzyNumber::FuzzyNumber(float f) {
	if (f<0 || f>1) throw runtime_error("invalid float fuzzy range");
	this->p = (int)(f * 1000.);
}

FuzzyNumber FuzzyNumber::operator+(const FuzzyNumber& f) {
	throw runtime_error("FuzzyNumber::operator+ not implemented.");
	return FuzzyNumber((float)0.5);
}
FuzzyNumber FuzzyNumber::operator-(const FuzzyNumber& f) {
	throw runtime_error("FuzzyNumber::operator- not implemented.");
	return FuzzyNumber((float)0.5);
}
FuzzyNumber FuzzyNumber::operator*(const FuzzyNumber& f) {
	throw runtime_error("FuzzyNumber::operator* not implemented.");
	return FuzzyNumber((float)0.5);
}
FuzzyNumber FuzzyNumber::operator/(const FuzzyNumber& f) {
	throw runtime_error("FuzzyNumber::operator/ not implemented.");
	return FuzzyNumber((float)0.5);
}
FuzzyNumber FuzzyNumber::operator&&(const FuzzyNumber& f) {
	if (f.p < this->p) return FuzzyNumber(f.p);
	return FuzzyNumber(this->p);
}
FuzzyNumber FuzzyNumber::operator||(const FuzzyNumber& f) {
	if (f.p > this->p) return FuzzyNumber(f.p);
	return FuzzyNumber(this->p);
}
FuzzyNumber FuzzyNumber::operator!() {
	return FuzzyNumber(1000 - this->p);
}

int FuzzyNumber::asInt() {
	return this->p;
}

ostream& operator<< (ostream& o, const FuzzyNumber& f) {
	 o << f.p;
	 return o;
}


} /* namespace xfuzz */
