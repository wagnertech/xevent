/*
 * FuzzyNumber.h
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#ifndef FUZZYNUMBER_H_
#define FUZZYNUMBER_H_

#include <iostream>

namespace xfuzz {

class FuzzyNumber {
public:
	FuzzyNumber() = delete;
	FuzzyNumber(int);
	FuzzyNumber(float);

	FuzzyNumber operator+(const FuzzyNumber& f);
	FuzzyNumber operator-(const FuzzyNumber& f);
	FuzzyNumber operator*(const FuzzyNumber& f);
	FuzzyNumber operator/(const FuzzyNumber& f);
	FuzzyNumber operator&&(const FuzzyNumber& f);
	FuzzyNumber operator||(const FuzzyNumber& f);
	FuzzyNumber operator!();

	int asInt();

	friend std::ostream& operator<< (std::ostream&, const FuzzyNumber&);

private:
	int p; // promille
};

} /* namespace xfuzz */

#endif /* FUZZYNUMBER_H_ */
