/*
 * RuleSystem.cpp
 *
 *  Created on: 06.09.2016
 *      Author: gnublin
 */

#include "RuleSystem.h"

#include "ActiveLeafUpdateException.h"
#include "Attribute.h"
#include "ConfigFile.h"
#include "ConstantLeaf.h"
#include "EventException.h"
#include "TimeLeaf.h"
#include "ValueLeaf.h"

#include "util.h"

#include <algorithm>
#include <sstream>

#include <memory.h> // memcpy
#include <sstream>

int xfuzz::RuleSystem::get(Event& e, Event& outEvent) {

	stringstream* ssp = nullptr;
	if (e.getDataLength() > 0 && e.getDataType() == 's') {
		if (memcmp(&e.getData(), "EXPLAIN", 7) == 0) {
			ssp = new stringstream;
		}
		else throw EventException("Unexpected input string", e);
	}
	try {
		FuzzyNumber f = this->rule_elems.at(this->start_node)->get(ssp);
		int promille = f.asInt();
		if (ssp) outEvent.pushData(ssp->str().c_str(), ssp->str().length(), 's');
		else outEvent.pushData((char*)&promille, sizeof(promille), 'i');
		return 0;
	}
	catch(ActiveLeafUpdateException& alue) {
		this->activeValueIdx = alue.getActiveValueIdx();
		return this->activeValueIdx;
	}
}

int xfuzz::RuleSystem::response(Event& event, Event& outEvent) {
	this->setEventData(this->activeValueIdx, event);
	return this->get(event, outEvent);
}

void xfuzz::RuleSystem::init(const EDCommon& edcommon, std::string name_of_rule_system) {
	string config_fn = edcommon.getConfigFileName();
	ConfigFile cf(config_fn);
	vector<string> rule_lines = cf.requireSectionData(name_of_rule_system);
	bool first_run = true;
	for (string& line: rule_lines) {
		stringstream ss(line);
		int no;
		string type;
		FuzzyElem* elem;
		util::split(ss,';', no, type);
		if (first_run) {
			this->start_node = no;
			first_run = false;
		}
		if (type == "ActiveValue") {
			string name;
			int utime;
			bool ok = util::split(ss,';', name, utime);
			if (! ok) throw runtime_error("RuleSystem: Active_value format error");
			elem = new ActiveValue(no, name, utime);
		} else if (type == "ValueLeaf") {
			int av_no;
			float thr1;
			float thr2;
			bool ok = util::split(ss, ';', av_no, thr1, thr2);
			if (! ok) throw runtime_error("RuleSystem: ActiveLeaf format error");
			elem = new ValueLeaf(no, &this->rule_elems, av_no,thr1,thr2);
		} else if (type == "TimeLeaf") {
			float t1;
			float t2;
			bool ok = util::split(ss, ';', t1, t2);
			if (! ok) throw runtime_error("RuleSystem: TimeLeaf format error");
			elem = new TimeLeaf(no, t1,t2);
		} else if (type == "ConstantLeaf") {
			int thr;
			bool ok = util::split(ss, ';', thr);
			if (! ok) throw runtime_error("RuleSystem: ConstantLeaf format error");
			elem = new ConstantLeaf(no, thr);
		} else {
			// must be an logical operation
			int n1,n2;
			bool ok = util::split(ss, ';', n1, n2);
			if (! ok) throw runtime_error("RuleSystem: logical expression format error");
			elem = new FuzzyNode(no, &this->rule_elems, type, n1, n2);
		}
		if (rule_elems.count(no)) throw runtime_error("RuleSystem: ID already occupied.");
		rule_elems[no] = elem;
	}
}

bool is_digits(const std::string &str)
{
    return all_of(str.begin(), str.end(), ::isdigit); // C++11
}

int xfuzz::RuleSystem::set(Event& event) {
	// check data type
	if (event.getDataType() != 'a') throw EventException("RuleSystem: data type 'a' expected", event);
	Attribute att(event);
	string name = att.getName();
	const Event& att_evt = att.getEvent();
	if (is_digits(name)) {
		int av_idx = util::toVal<int>(name);
		this->setEventData(av_idx, att_evt);
	}
	else this->setEventData(name, att_evt);
	return 0;
}

void xfuzz::RuleSystem::setEventData(int av_idx, const Event &event) {
	float value;
	if (event.getDataType() != 'f') throw EventException("data type f expected", event);
	memcpy(&value, &event.getData(), sizeof(float));
	if (this->rule_elems.count(av_idx) == 0) throw EventException(("Invalid av_idx: "+to_string(av_idx)).c_str(), event);
	ActiveValue* av = dynamic_cast<ActiveValue*>(this->rule_elems[av_idx]);
	if (!av) throw EventException(("No ActiveValue at av_idx: "+to_string(av_idx)).c_str(), event);
	av->setValue(value);
}

void xfuzz::RuleSystem::setEventData(string name, const Event &event) {
	bool found = false;
	for (auto p : this->rule_elems) {
		ActiveValue* av = dynamic_cast<ActiveValue*>(p.second);
		if (av) {
			if (av->getName() == name) {
				found = true;
				this->setEventData(p.first, event);
			}
		}
	}
	if (!found) throw EventException(("Invalid AV name: "+name).c_str(), event);
}
