/*
 * RuleSystem.h
 *
 * RuleSystem represents a set of fuzzy rules. The rules are
 * defined inside the xevent init file. A rule section has the following
 * format:
 * [NAME_OF_RULE_SYSTEM]
 * ID;TYPE[;TYPE_SPECIFIC_DATA]
 *
 * ID: int number unique for that line
 * TYPE: ActiveValue | ValueLeaf | TimeLeaf | ConstantLeaf | OR | AND
 * TYPE_SPECIFIC_DATA for these TYPEs:
 * ActiveValue: NAME;0 | valid time
 * ValueLeaf: ActiveValue-ID;ZERO_LEVEL;ONE_LEVEL
 * TimeLeaf: ZERO_TIME;ONE_TIME
 * ConstantLeaf: VALUE
 * OR/AND: Op1_ID;Op2_ID
 *
 * The element with ID=1 is asked for its value, when the rule system is evaluated.
 */

#ifndef RULESYSTEM_H_
#define RULESYSTEM_H_

#include "ActiveValue.h"
#include "EDCommon.h"
#include "FuzzyElem.h"

#include <map>
#include <string>
#include "ValueLeaf.h"

namespace xfuzz {

class RuleSystem: public HandledObject {
public:
	virtual int get(Event&, Event&);
		// returns the FuzzyNumber of the RuleSystem

	virtual int response(Event&, Event&);
		// updates the waiting ActiveLeaf and returns the FuzzyNumber of the RuleSystem

	virtual int set(Event&);

protected:
	int activeValueIdx;
	std::map<int,FuzzyElem*> rule_elems;
	int start_node;

	void init(const EDCommon&, std::string name_of_rule_system);
	void setEventData(std::string name, const Event &event);
	void setEventData(int av_idx, const Event &event);
};

} /* namespace xfuzz */

#endif /* RULESYSTEM_H_ */
