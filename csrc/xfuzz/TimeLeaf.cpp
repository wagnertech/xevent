/*
 * FuzzyLeaf.cpp
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#include "TimeLeaf.h"

#include <ctime>
#include <sstream>

namespace xfuzz {

FuzzyNumber TimeLeaf::get(std::stringstream* ssp) {
	// get time as float 0 .. 24
	time_t raw_time = time(0);
	struct tm* curr_time = localtime(&raw_time);
	float t = curr_time->tm_hour + curr_time->tm_min/60.;

	// calc fuzzy value
	// 0 value: to from-ramp
	// 1 value: from -> to
	// 0 level: from to+ramp

	// special overnight handling
	if (t < from-ramp) t = t+24;
	float to1 = to;
	if (to1 < from) to1 = to1+24;
	if (from <= t && t <= to1) this->f = FuzzyNumber(1000);
	else if (t > to+ramp) this->f = FuzzyNumber(0);
	else if (t < from) this->f = FuzzyNumber((t-from+ramp) / ramp);
	else this->f = FuzzyNumber((t-to) / ramp);
	if (ssp) *ssp <<"<TL id=\"" <<this->my_no <<"\">" <<this->f <<"</TL>";
	return this->f;
}

} /* namespace xfuzz */
