/*
 * FuzzyLeaf.h
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#ifndef TIMELEAF_H_
#define TIMELEAF_H_

#include "FuzzyNode.h"

namespace xfuzz {

class TimeLeaf: public FuzzyElem {
public:
	TimeLeaf(int my_no, float from, float to)
		:FuzzyElem(my_no), from(from), to(to), ramp(1.)  {}

	virtual FuzzyNumber get(std::stringstream* ssp = nullptr);

private:
	float from;
	float to;
	float ramp; // duration for fuzzy values
	// 0 value: to from-ramp
	// 1 value: from -> to
	// 0 level: from to+ramp
};

} /* namespace xfuzz */

#endif /* TIMELEAF_H_ */
