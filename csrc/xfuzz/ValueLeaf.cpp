/*
 * ActiveLeaf.cpp
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#include "EventException.h"
#include "ValueLeaf.h"

#include <sstream>

namespace xfuzz {

FuzzyNumber ValueLeaf::get(std::stringstream* ssp) {
	if (!this->rule_elems->count(this->av)) throw EventException(("ValueLeaf: No entry at av_idx: "+to_string(av)).c_str());
	ActiveValue* avp = dynamic_cast<ActiveValue*>(this->rule_elems->at(this->av));
	if (!avp) throw EventException(("ValueLeaf: No ActiveValue at av_idx: "+to_string(av)).c_str());
	float lastValue = avp->getValue();

	// calculate fuzzy value
	if (this->zeroLevel < this->oneLevel) {
		// ramp up
		if (lastValue < this->zeroLevel) this->f = FuzzyNumber(0);
		else if (lastValue > this->oneLevel) this->f = FuzzyNumber(1000);
		else this->f = FuzzyNumber((lastValue-this->zeroLevel)/(this->oneLevel-this->zeroLevel));
	}
	else {
		// ramp down
		if (lastValue < this->oneLevel) this->f = FuzzyNumber(1000);
		else if (lastValue > this->zeroLevel) this->f = FuzzyNumber(0);
		else this->f = FuzzyNumber((this->zeroLevel-lastValue)/(this->zeroLevel-this->oneLevel));
	}

	if (ssp) *ssp <<"<VL id=\"" <<this->my_no <<"\">" <<this->f <<"</VL>";
	return this->f;
}


} /* namespace xfuzz */
