/*
 * ActiveLeaf.h
 *
 *  Created on: 05.09.2016
 *      Author: gnublin
 */

#ifndef VALUELEAF_H_
#define VALUELEAF_H_

#include "FuzzyElem.h"

#include "ActiveValue.h"

namespace xfuzz {

class ValueLeaf: public FuzzyElem {
public:
	ValueLeaf(int my_no, t_rule_elems* re, int av, float zeroLevel, float oneLevel)
		:FuzzyElem(my_no, re), av(av), zeroLevel(zeroLevel), oneLevel(oneLevel) {}

	virtual FuzzyNumber get(std::stringstream* ssp = nullptr);

private:
	int av;
	float zeroLevel;
	float oneLevel;
};

} /* namespace xfuzz */

#endif /* VALUELEAF_H_ */
