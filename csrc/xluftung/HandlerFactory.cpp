/*
 * HandlerFactory.cpp
 *
 *  Created on: 29.01.2014
 *      Author: gnublin
 */
#include "HandlerFactory.h"

#include "LuftungRegelwerk.h"
#include "Luftung.h"
#include "EventException.h"

#include <stdexcept>
#include <string>

HandledObject& HandlerFactory::getInstance(int classs, int inst) {
	string class_name = this->edCommon->getClassName(classs);
	if (class_name == "LuftungRegelwerk") return LuftungRegelwerk::getInstance(*this->edCommon);
	else if (class_name == "Luftung") return Luftung::getInstance();
	else {
		string msg("xluftung: unknown class name: "+class_name);
		throw EventException(msg.c_str());
	}
}

void HandlerFactory::setClassIds(){
	; // not used
}
