/*
 * Luftung.cpp
 *
 *  Created on: 23.06.2021
 *      Author: debian10
 */

#include "Luftung.h"

#include <sstream>

#include <string.h>

Luftung Luftung::theInstance;

Luftung& Luftung::getInstance() {
	return Luftung::theInstance;
}

int Luftung::trigger(Event& in, Event& out) {
	return 1;
}

int Luftung::response(Event& in, Event& out) {
	int data;
	memcpy(&data, &in.getData(), sizeof(int));
	stringstream cmd;
	cmd << hex << "groupwrite ip:127.0.0.1 3/0/2 " << data*255/1000;
	out.pushData(cmd.str().c_str(), cmd.str().length(), 's');
	return 0;
}
