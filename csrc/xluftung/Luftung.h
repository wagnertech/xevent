/*
 * Luftung.h
 *
 *  Created on: 23.06.2021
 *      Author: debian10
 */

#ifndef LUFTUNG_H_
#define LUFTUNG_H_

#include <HandledObject.h>

class Luftung: public HandledObject {
public:
	static Luftung& getInstance();
	virtual int trigger(Event&, Event&);
	virtual int response(Event&, Event&);

	static Luftung theInstance;
};

#endif /* LUFTUNG_H_ */
