/*
 * LuftungRegelwerk.cpp
 *
 *  Created on: 23.06.2021
 *      Author: debian10
 */

#include "LuftungRegelwerk.h"

#include "ConstantLeaf.h"
#include "FuzzyNode.h"
#include "TimeLeaf.h"

using namespace xfuzz;

LuftungRegelwerk* LuftungRegelwerk::theInstance;

LuftungRegelwerk::LuftungRegelwerk(const EDCommon& edcommon) {
	this->init(edcommon, "RegelwerkLuftung");
}

LuftungRegelwerk& LuftungRegelwerk::getInstance(const EDCommon& edcommon) {
	if (! LuftungRegelwerk::theInstance) LuftungRegelwerk::theInstance = new LuftungRegelwerk(edcommon);
	return *LuftungRegelwerk::theInstance;
}
