/*
 * LuftungRegelwerk.h
 *
 *  Created on: 23.06.2021
 *      Author: debian10
 */

#ifndef LUFTUNGREGELWERK_H_
#define LUFTUNGREGELWERK_H_

#include "RuleSystem.h"

class LuftungRegelwerk : public xfuzz::RuleSystem{
public:
	LuftungRegelwerk(const EDCommon& edcommon);

	static LuftungRegelwerk& getInstance(const EDCommon& edcommon);

private:
	static LuftungRegelwerk* theInstance;
};

#endif /* LUFTUNGREGELWERK_H_ */
