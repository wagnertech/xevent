cmake_minimum_required(VERSION 3.13)

#set (CMAKE_MODULE_PATH $ENV{CMAKE_MODULE_PATH})
# include toolset definition
#include(toolset OPTIONAL)
#include(toolset)

# libutil.a
FILE(GLOB SRC_FILES csrc/util/*.cpp)
add_library(util SHARED ${SRC_FILES})

# libtimerhandler.so
FILE(GLOB SRC_FILES csrc/timerhandler/*.cpp)
add_library(timerhandler SHARED ${SRC_FILES})
target_include_directories(timerhandler PRIVATE csrc/util)
target_include_directories(timerhandler PRIVATE csrc/sarah)

# libhandledobject.so
FILE(GLOB SRC_FILES csrc/handledobject/*.cpp)
add_library(handledobject SHARED ${SRC_FILES})
target_include_directories(handledobject PRIVATE csrc/util)

# heartbeatd
FILE(GLOB SRC_FILES csrc/heartbeatd/*.cpp)
add_executable(heartbeatdd ${SRC_FILES})
target_include_directories(heartbeatdd PRIVATE csrc/util)
target_link_libraries(heartbeatdd util)

# objecthandlerd
FILE(GLOB SRC_FILES csrc/objecthandlerd/*.cpp)
add_executable(objecthandlerdd ${SRC_FILES})
target_include_directories(objecthandlerdd PRIVATE csrc/util)
target_link_libraries(objecthandlerdd util)
target_link_libraries(objecthandlerdd handledobject)

# xsendevent
FILE(GLOB SRC_FILES csrc/sendhandlerevent/*.cpp)
add_executable(xsendevent ${SRC_FILES})
target_include_directories(xsendevent PRIVATE csrc/util)
target_link_libraries(xsendevent util)

