#!/bin/bash
set -e

paket=$1

mkdir -p $paket/opt/xeventhandler/
cp ~/build/objecthandlerdd $paket/opt/xeventhandler/objecthandlerd
cp ~/build/libhandledobject.so $paket/opt/xeventhandler/
cp ~/build/libutil.so $paket/opt/xeventhandler/
cp ~/build/csrc/objecthandlerd/etc/simpletest.conf $paket/opt/xeventhandler/
mkdir -p $paket/opt/timerhandler/
cp ~/build/libtimerhandler.so $paket/opt/timerhandler/
ln -sf /opt/eventhandler/objecthandlerd $paket/opt/timerhandler/timerhandlerd
ln -sf /opt/eventhandler/libutil.so $paket/opt/timerhandler/
pushd $paket/opt/timerhandler >/dev/null; ln -sf libtimerhandler.so libhandledobject.so; popd >/dev/null
cp ~/build/heartbeatdd $paket/opt/timerhandler/heartbeatd
mkdir -p $paket/usr/bin/
cp ~/build/xsendevent $paket/opt/xeventhandler/
cp ~/build/csrc/sendhandlerevent/xsendevent.sh $paket/usr/bin/xsendevent
mkdir -p $paket/etc/init.d
cp ~/build/csrc/objecthandlerd/etc/xevent-handler $paket/etc/init.d

# copy man pages
mkdir -p $paket/usr/share/man/man5
mkdir -p $paket/usr/share/man/man7
gzip -c doc/xevent.conf.5 >$paket/usr/share/man/man5/xevent.conf.5.gz
gzip -c doc/xevent-handler.7 > $paket/usr/share/man/man7/xevent-handler.7.gz

