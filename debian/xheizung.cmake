cmake_minimum_required(VERSION 3.13)

# libutil.so
FILE(GLOB SRC_FILES csrc/util/*.cpp)
add_library(util SHARED ${SRC_FILES})

# libsarah.so
FILE(GLOB SRC_FILES csrc/sarah/*.cpp)
add_library(sarah SHARED ${SRC_FILES})
target_include_directories(sarah PRIVATE csrc/util)
target_include_directories(sarah PRIVATE csrc/xfuzz)
target_link_libraries(sarah util)
target_link_libraries(sarah xfuzz)
target_link_libraries(sarah usb)

# xfuzz.a
add_compile_options(-fPIC)
FILE(GLOB SRC_FILES csrc/xfuzz/*.cpp)
add_library(xfuzz STATIC ${SRC_FILES})
target_include_directories(xfuzz PRIVATE csrc/util)
target_link_libraries(xfuzz util)

