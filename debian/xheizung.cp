#!/bin/bash
set -e

paket=$1

mkdir -p $paket/opt/xheizung/
cp ~/build/libsarah.so $paket/opt/xheizung/
cp ~/build/csrc/sarah/etc/ledtest.conf $paket/opt/xheizung/
cp ~/build/csrc/sarah/etc/xheizung.conf $paket/opt/xheizung/
cp ~/build/csrc/sarah/etc/fuzzytest.conf $paket/opt/xheizung/
#ln -s /opt/xeventhandler/objecthandlerd $paket/opt/xheizung/xheizung
pushd $paket/opt/xheizung >/dev/null; ln -sf libsarah.so libhandledobject.so; popd >/dev/null

