cmake_minimum_required(VERSION 3.13)

# libutil.so
FILE(GLOB SRC_FILES csrc/util/*.cpp)
add_library(util SHARED ${SRC_FILES})

# libxluftung.so
FILE(GLOB SRC_FILES csrc/xluftung/*.cpp)
add_library(xluftung SHARED ${SRC_FILES})
target_include_directories(xluftung PRIVATE csrc/util)
target_include_directories(xluftung PRIVATE csrc/xfuzz)
target_link_libraries(xluftung util)
target_link_libraries(xluftung xfuzz)

# xfuzz.a
add_compile_options(-fPIC)
FILE(GLOB SRC_FILES csrc/xfuzz/*.cpp)
add_library(xfuzz STATIC ${SRC_FILES})
target_include_directories(xfuzz PRIVATE csrc/util)
target_link_libraries(xfuzz util)

