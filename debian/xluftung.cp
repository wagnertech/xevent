#!/bin/bash
set -e

paket=$1

mkdir -p $paket/opt/xluftung/
cp ~/build/libxluftung.so $paket/opt/xluftung/
cp ~/build/csrc/xluftung/etc/xluftung.conf $paket/opt/xluftung/
pushd $paket/opt/xluftung >/dev/null; ln -sf libxluftung.so libhandledobject.so; popd >/dev/null

